<?php

namespace Drupal\coop003_login_handler\Services;

use GuzzleHttp\ClientInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Serialization\Json;

/**
 * Class.
 *
 * @package Drupal\coop003_login_handler\Services\LoginManagedService
 */
class LoginManagedService {

  /**
   * Minimum value of PHP memory_limit for SimpleTest.
   */
  const DOMAIN_EMAIL = '@cooperativa.com.pa';

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * Constructs a new LoginInterceptService object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ClientInterface $httpClient, $factory, $config) {
    $this->httpClient = $httpClient;
    $this->entityTypeManager = $entity_type_manager;
    $this->loggerFactory = $factory;
    $this->config = $config;
  }

  /**
   * Implements Method to send request to the api.
   *
   * Return bolean.
   */
  public function requestUserAgaintsApi($username, $password) {
    $url = \Drupal::config('coop003_login_handler.settings')->get('api_url');
    $response = $this->httpClient->post($url, [
      'verify' => TRUE,
      'form_params' => [
        'username' => $username,
        'password' => $password,
      ],
      'headers' => [
        'Content-type' => 'application/x-www-form-urlencoded',
      ],
      'json' => Json::decode($params),
    ])->getBody()->getContents();

    $object = json_decode($response);

    if ($object->resabit == '1001') {
      return TRUE;

    }
    elseif ($object->resabit == '1002') {
      return FALSE;

    }
    else {
      $type = $object->usuario->tipo;
      $type = $this->mapRoleTypes($type);
      $this->userCreateOrUpdate($username, $password, $type);

    }

    return TRUE;
  }

  /**
   * Validate if user exists.
   *
   * Return $id.
   */
  public function validateUser($username = NULL) {
    $id = NULL;
    if ($username !== NULL) {
      $id = $this->entityTypeManager->getStorage('user')->getQuery()
        ->condition('status', 1)
        ->condition('name', $username)
        ->execute();
    }

    return $id;
  }

  /**
   * Create user.
   */
  public function createUser($username, $password, $role) {
    $email = $username . DOMAIN_EMAIL;
    // Create user object.
    $user = $this->entityTypeManager->getStorage('user')->create();
    $user->setPassword($password);
    $user->enforceIsNew();
    $user->setEmail($email);
    $user->setUsername($username);
    $user->set('field_user_external', TRUE);
    $user->activate();

    try {
      $success = $user->save();
    }
    catch (Exeption $e) {
      $this->loggerFactory->get('user')->error($e->getMessage());
    }

    if ($success && $role) {

      try {
        $user->addRole($role);
        $user->save();
      }
      catch (Exeption $e) {
        $this->loggerFactory->get('user')->error($e->getMessage());
      }

    }
  }

  /**
   * Handle user for create or update.
   */
  public function userCreateOrUpdate($username = NULL, $password = "", $type = FALSE) {
    $role = $this->mapRoleTypes($type);
    $user = user_load_by_name($username);
    if (empty($user)) {
      $this->createUser($username, $password, $role);

    }
    else {
      $current_user = $this->entityTypeManager->getStorage('user')->load($user->id);

      if ($current_user) {
        $current_user->setPassword($password);
        $current_user->set('field_user_external', TRUE);

        if ($role) {
          $current_user->addRole($role);
        }
        $current_user->save();
      }
    }
  }

  /**
   * Match roles from api to roles in system.
   */
  private function mapRoleTypes($type) {
    $roles = ["anonymous", "asociado", "tercero"];
    $key = array_search(strtolower($type), $roles);
    if ($roles[$key] !== 'anonymous') {
      return $roles[$key];

    }

    return FALSE;
  }

}
