document.addEventListener('DOMContentLoaded', () => {
    // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach( el => {
      el.addEventListener('click', () => {

        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }
});

(function ($, Drupal) {
  'use strict';
  $(".fa-angle-right").on("click", function () {
    var item = $(".paragraph--type--navigation-menu ul.menu>li:first");
    item.parent().append(item);
    return false;
  });
  
  $(".fa-angle-right").on("click", function () {
    var item = $(".view-display-id-block_products_menu .item-list > ul > li:first");
    item.parent().append(item);
    return false;
  });
  
  $(".view-events .view-filters .fa-list-ul").on("click", function () {
    $(".view-display-id-block_month").fadeOut(function () {
      $(".view-display-id-block_list").fadeIn();
    })
  });
  
  $(".view-events .view-filters .fa-calendar").on("click", function () {
    $(".view-display-id-block_list").fadeOut(function () {
      $(".view-display-id-block_month").fadeIn();
    })
  });
  
})(jQuery, Drupal);