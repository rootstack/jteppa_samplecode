'use strict';

const pkg = require('./package.json');
const fractal = require('@frctl/fractal').create();
const mandelbrot = require('@frctl/mandelbrot');
const twigAdapter = require('@wondrousllc/fractal-twig-drupal-adapter');
const twig = twigAdapter({
  handlePrefix: '@components/',
});

const paths = {
  build: `${__dirname}/dist`,
  src: `${__dirname}/styleguide-src`,
  static: `${__dirname}/styleguide`,
};

const cooperativaTheme = mandelbrot({
  //favicon: './assets/favicons/favicon.ico',
  lang: 'en-us',
  skin: 'navy',
  //logo: './assets/images/agropur_logo_small.png',
  nav: ["docs", "components"],
  static: {
    mount: 'fractal',
  },
});

const mdAbbr = require('markdown-it-abbr');
const mdFootnote = require('markdown-it-footnote');
const md = require('markdown-it')({
  html: true,
  xhtmlOut: true,
  typographer: true,
}).use(mdAbbr).use(mdFootnote);
const nunjucksDate = require('nunjucks-date');
const nunjucks = require('@frctl/nunjucks')({
  filters: {
    date: nunjucksDate,
    markdown(str) {
      return md.render(str);
    },
    markdownInline(str) {
      return md.renderInline(str);
    },
    slugify(str) {
      return str.toLowerCase().replace(/[^\w]+/g, '');
    },
    stringify() {
      return JSON.stringify(this, null, '\t');
    },
  }
  // paths: [`${paths.static}/assets/vectors`],
});

// specify a directory to hold the theme override templates
//cooperativaTheme.addLoadPath(__dirname + '/styleguide-src/fractal-theme'); //<-- this is missing ??

// Project config
fractal.set('project.title', pkg.name);
fractal.set('project.version', pkg.version);
fractal.set('project.author', pkg.author);

// Components config
fractal.components.engine(twig);
fractal.components.set('default.preview', '@preview');
fractal.components.set('default.status', null); //<-- added
fractal.components.set('ext', '.twig');
fractal.components.set('path', `${paths.src}/components`);

// Docs config
fractal.docs.engine(nunjucks);
fractal.docs.set('ext', '.md');
fractal.docs.set('path', `${paths.src}/docs`);

// Web UI config
fractal.web.theme(cooperativaTheme);
fractal.web.set('static.path', paths.static);
fractal.web.set('builder.dest', paths.build);
// fractal.web.set('server.sync',true); // browsersync

// Export config
module.exports = fractal;