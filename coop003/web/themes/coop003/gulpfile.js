'use strict';

const fractal = require('./fractal.js');
const logger = fractal.cli.console; // keep a reference to the fractal CLI console utility

var gulp = require('gulp');
var sass = require('gulp-sass');
var babel = require('gulp-babel');
var livereload = require('gulp-livereload');
var postcss = require('gulp-postcss');
var reporter = require('postcss-reporter');
var jsonToSass = require('gulp-json-to-sass');
//var autoprefixer = require('autoprefixer');
var combineMq = require('gulp-combine-mq');
var imagemin = require('gulp-imagemin');
//var cssGlobbing = require('gulp-css-globbing');
//var spritesmith = require('gulp.spritesmith');
var concat = require("gulp-concat");
var notify = require("gulp-notify");
var rename      = require('gulp-rename');
var sourcemaps  = require('gulp-sourcemaps');
var stylelint = require('stylelint');
// var stylelintConfig = require('./scss/stylelintrc.json');
//var scss = require("postcss-scss");
//var es = require('event-stream');

var sassGlob = require('gulp-sass-glob');

var paths = {
  theme_root : './',
  components_scss: './styleguide-src/components/{01-atoms,02-molecules,03-organisms,04-global}/**/*.scss',
  components_js: './js/components/*.js',
  global_js: './js/**/*.js',
  styles: './scss/styles.scss',
  styles_src: './scss/**/*.scss',
  stylguide_styles: './scss/styleguide.scss',
  bulma_sass: './node_modules/bulma/bulma.sass'
};

// Error notifications with notify
// Shows a banner on macOs
var reportError = function(error) {
  notify({
    title: 'Gulp Task Error',
    message: 'Check the console.'
  }).write(error);
  console.log(error.toString());
  this.emit('end');
};

// Minify jpg, png, gif, svg
gulp.task('images', () =>
  gulp.src('./assets/images/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./styleguide/assets/images'))
);

// Generate fonts for Fractal
gulp.task('fonts', function () {
  gulp.src('./assets/fonts/**/*')
  .pipe(gulp.dest('./styleguide/assets/fonts'))
});

// Compile JavaScript for Fractal
gulp.task('js', function () {
  return gulp.src(paths.global_js)
    .pipe(babel())
    .pipe(rename(function (path) {
      path.basename = path.basename.replace(/\.es6/, '');
      return path;
    }))
    .pipe(gulp.dest('./styleguide/js'))
    .pipe(livereload());
});

// Compile JavaScript for Fractal
gulp.task('js-build', function () {
  return gulp.src(paths.components_js)
    .pipe(babel())
    .pipe(rename(function (path) {
      path.basename = path.basename.replace(/\.es6/, '');
      return path;
    }))
    .pipe(gulp.dest('./js/es6/components/'))
    .pipe(livereload());
});



/* Move js related files.
gulp.task('js-related-files', function() {
  var hammerJsMap = gulp.src('./js/vendor/hammerjs/hammer.min.js.map')
      .pipe(gulp.dest('./styleguide/js/vendor/hammerjs'));

  var slickCss = gulp.src('./js/vendor/slick/slick.css')
      .pipe(gulp.dest('./styleguide/js/vendor/slick'));
  
  return es.concat(hammerJsMap, slickCss);
});
*/
// Lint Styles
gulp.task('lint-styles', function() {
  return gulp.src(paths.styles_src)
  .pipe(postcss([
    stylelint(stylelintConfig),
    reporter({clearMessages: true})
    ],
    {
        syntax: scss
    }
  ));
});

// Sass compilation, exports one .css for Fractal and one for Drupal
gulp.task('sass', function () {
  return gulp.src(paths.styles)
    .pipe(sassGlob())
    .pipe(sass({
      errLogToConsole: false,
      sourceComments: true,
      precision: 3,
      outputStyle: 'nested'
    }).on('error', sass.logError))
    .pipe(combineMq({
      beautify: false // false will inline css
    }))
    // .pipe(jsonToSass({
    //   jsonPath: `${paths.theme_root}/scss/variables/breakpoints.json`,
    //   scssPath: `${paths.theme_root}/scss/utilities/_breakpoints.scss`
    // }))
    // .pipe(jsonToSass({
    //   jsonPath: `${paths.theme_root}/scss/variables/colors.json`,
    //   scssPath: `${paths.theme_root}/scss/utilities/_colors.scss`
    // }))
    // .pipe(jsonToSass({
    //   jsonPath: `${paths.theme_root}/scss/variables/fonts.json`,
    //   scssPath: `${paths.theme_root}/scss/utilities/_fonts.scss`
    // }))
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./styleguide/css'))
    .pipe(gulp.dest('./css'))
    //.pipe(livereload());
});

// Sass compilation, exports one .css for the styleguide structure page
gulp.task('sass-styleguide', function () {
  return gulp.src(paths.stylguide_styles)
    .pipe(sourcemaps.init())
    .pipe(concat('styleguide.scss'))
    .pipe(sass({
      errLogToConsole: false,
      sourceComments: true,
      precision: 3,
      outputStyle: 'nested'
    })
    .on('error', sass.logError))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./styleguide/css'))
    .pipe(gulp.dest('./css'))
    //.pipe(livereload());
});



/*
 * Start the Fractal server
 *
 * In this example we are passing the option 'sync: true' which means that it will
 * use BrowserSync to watch for changes to the filesystem and refresh the browser automatically.
 * Obviously this is completely optional!
 *
 * This task will also log any errors to the console.
 */

gulp.task('fractal:start', function(){
    const server = fractal.web.server({
        sync: true
    });
    server.on('error', err => logger.error(err.message));
    return server.start().then(() => {
        logger.success(`Fractal local server: ${server.url}`);
        logger.success(`Network url: ${server.urls.sync.external}`);
    });
});

/*
 * Run a static export of the project web UI.
 *
 * This task will report on progress using the 'progress' event emitted by the
 * builder instance, and log any errors to the terminal.
 *
 * The build destination will be the directory specified in the 'builder.dest'
 * configuration option set above.
 */

gulp.task('fractal:build', function(){
    const builder = fractal.web.builder();
    builder.on('progress', (completed, total) => logger.update(`Exported ${completed} of ${total} items`, 'info'));
    builder.on('error', err => logger.error(err.message));
    return builder.build().then(() => {
        logger.success('Fractal build completed!');
    });
});

/**
 * Build tasks
 */

// Watch sass files and update css folder
gulp.task('default', ['watch']);

// Watch sass files & generate styleguide
gulp.task('watch', ['images','fonts', 'sass', 'sass-styleguide','js','js-build','fractal:start'], function() {
  // Start watching changes and update styleguide & theme css file whenever changes are detected
  // Styleguide automatically detects existing server instance
  livereload.listen();
  gulp.watch(paths.styles_src, ['sass']);
  gulp.watch(paths.components_js, ['js']);
  // reload full page when templates changes
  gulp.watch(paths.templates, function (files){
  livereload.changed(files)
    });
});

gulp.task('build', ['images', 'fonts', 'sass', 'sass-styleguide', 'js', 'js-build'], function() {
  gulp.start('fractal:build');
});

gulp.task('prod', ['images', 'fonts', 'sass', 'js', 'js-build'], function() {
});
