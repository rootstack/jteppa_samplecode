'use strict';
/**
 * @ngdoc function
 * @name caldeyroApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the caldeyroApp
 */

/*jshint latedef: nofunc */
angular.module('caldeyroApp')
  .controller('MainCtrl', MainCtrl);

MainCtrl.$inject = [
  '$state',
  'Facets',
  '$scope',
  'Search',
  '$q',
  'screenSize',
  '$rootScope',
  '$document',
  '$location',
  '$anchorScroll',
  '$stateParams',
  'MetaTags',
  'SearchService'
];

function MainCtrl(
  $state,
  Facets,
  $scope,
  Search,
  $q,
  screenSize,
  $rootScope,
  $document,
  $location,
  $anchorScroll,
  $stateParams,
  MetaTags,
  SearchService
) {
  /*@ngInject*/
  var main = this;
  $rootScope.title = $rootScope.translate("search_for_properties_caldeyro_victorica");
  $rootScope.searchResults = true;
  $rootScope.isFirstLoad = false;
  $rootScope.showSmall = false;

  $scope.$on('langChanged', function() {
    //activate(true);
  });

  $scope.$on('reloadSearch', function() {
    if (bigHeader) {
      main.headerType = 'small';
      bigHeader = false;
      $rootScope.showSmall = true;
    }
    // search(true, false);
    updateFiltersUrl();
  });

  main.resultText = {};

  var bigHeader = true;

  var clearFacets = function() {
    $rootScope.searching = true;
    angular.forEach(main.categories, function(category) {
      category.clear();
    }, main);
    search(true);
  };

  var activate = function(force) {
    $rootScope.searchResults = true;

    Facets.getAll(force || false, { 'facet_name': 'all', lang: $rootScope.lang }).then(function(result) {
      main.categories = result.categories;
      getMainFiltersFromUrl();
      if (SearchService.location) {
        $location.search('filter[field_propiedad_ubicacion]', null);
        $location.search('filter[field_propiedad_ubicacion]', SearchService.location.id + "");
      } 
      else {
        var params = $location.search();
        if (typeof params['filter[field_propiedad_ubicacion]'] === 'undefined') {
          $location.search('filter[field_propiedad_ubicacion]', "61");
        }
      }

      buildFacetsFromUrl();
      search(force || false);
    });
  };

  var updateCategories = function(facets) {
    var category = null;
    _.forEach(_.keys(facets), function(categoryKey) {
      category = _.find(main.categories, { key: categoryKey });
      if (category) {
        category.update(facets[categoryKey]);
        $scope.$applyAsync(true);
      }
    });
  };

  function buildFacetsFromUrl() {
    var regExp = /\[([^)]+)\]/;
    var matches = '';
    var params = $location.search();
    var facets = _.pickBy(params, function(value, key) {
      return _.startsWith(key, 'filter') && key !== 'filter[language]';
    });
    var facetKey = '';
    var tmp = [];
    var category = undefined;
    var values = undefined;
    var tmpItem = undefined;
    _.forEach(facets, function(value, facet) {
      tmp = regExp.exec(facet);
      if (!_.isEmpty(tmp) && _.isArray(tmp) && tmp.length > 0) {
        facetKey = tmp[1];
        category = _.find(main.categories, { key: facetKey });
        if (!_.isEmpty(category) && !_.isEmpty(category.key)) {
          values = value.split(',');
          _.forEach(values, function(value) {
            if (category.key === 'field_propiedad_ubicacion' || category.key === 'field_propiedad_categoria') {
              tmpItem = _.find(category.flatItems, { tid: value });
            } else {
              tmpItem = _.find(category.flatItems, { filter: value });
            }
            if (!_.isEmpty(tmpItem)) {
              tmpItem.mark();
            }
          });
        }
      }
    });
    
    var sort = _.pickBy(params, function(value, key) {
      return _.startsWith(key, 'sort');
    });

    _.forEach(sort, function(value, sort) {
      tmp = regExp.exec(sort);
      if (!_.isEmpty(tmp) && _.isArray(tmp) && tmp.length > 0) {
        var sortKey = tmp[1];
        main.sort = _.find(main.sortList, { key: sortKey + '_' + value });
      }
    });

    if (params.items_per_page) {
      main.pager.size = parseInt(params.items_per_page, 10);
    }

    if (params.page) {
      main.pager.page = parseInt(params.page, 10) + 1;
    }

    if (params.key) {
      SearchService.text = params.key;
      $stateParams.keyword = params.key;
    }
  }

  var refRegEx = /\d{3,4}-[A-Za-z]{2}/g;
  
  var buildSearchParams = function() {
    $location.search({});
    var params = {};
    var urlParams = [];

    if (refRegEx.test($stateParams.keyword)) {
      clearFacets();
      SearchService.reset = false;
      params['key'] = $stateParams.keyword;
    } 
    else {
      params = { 'items_per_page': main.pager.size, page: main.pager.page - 1 };
      
      if (SearchService.reset) {
        clearFacets();
        SearchService.reset = false;
      } 
      else {
        angular.forEach(main.categories, function(category) {
          var _tmpValues = category.getValues();
          _tmpValues = _tmpValues.filter(function(e) { return e; });
          if (_tmpValues.length > 0) {
            params['filter[' + category.key + ']'] = _tmpValues.join(',');
          }
        }, main);

        if ($stateParams.keyword) {
          params['key'] = $stateParams.keyword;
        }
        params['sort[' + main.sort.value + ']'] = main.sort.order;
      }
    }

    params['filter[language]'] = $rootScope.lang;

    urlParams = angular.copy(params);

    delete urlParams['filter[field_propiedad_ubicacion]'];
    delete urlParams['filter[field_estado]'];
    delete urlParams['filter[field_propiedad_categoria]'];
    delete urlParams['filter[language]'];

    if (urlParams['items_per_page'] == 20) {
      delete urlParams['items_per_page'];
    }

    if (urlParams['page'] == 0) {
      delete urlParams['page'];
    }

    if (urlParams['sort[created]'] == "DESC") {
      delete urlParams['sort[created]'];
    }
    
    angular.forEach(urlParams, function(value, key) {
      $location.search(key, value);
    });
    
    $location.replace();
    //params['filter[type]'] ='propiedad';
    return params;
  };

  var search = function(force, skipTop) {
    $rootScope.searching = true;

    if (screenSize.is('xs')) {
      $rootScope.facetAnimate = false;
    }

    $rootScope.searchResults = true;
    // window.prerenderReady = false;
    main.properties = [];

    Search.query(force, buildSearchParams()).then(function(result) {
      $rootScope.searching = false;

      main.properties = _.map(result.data, function(property) {
        property.area = findPropertyArea(property);
        return property;
      });

      main.pager.count = result.count;
      main.resultText.total = main.pager.count;
      main.resultText.from = result.count > 0 ? ((main.pager.page - 1) * main.pager.size) + 1 : 0;
      main.resultText.to = result.count > 0 ? (main.resultText.from - 1) + main.properties.length : 0;

      updateCategories(result.facets);
      //MetaTags.setAll(result.metatag);
      updateFiltersUrl();
      updateMetaDescriptions();

      $rootScope.ogkeywords = result.metatag['Keywords'] || '';
      //$rootScope.ogdescription = result.metatag['Description'] || '';
      //$rootScope.title = result.metatag['Page title'] || '';

      if (!skipTop) {
        main.goToTop();
      }

      window.prerenderReady = true;
    });
  };

  var textSearch = function() {
    window.prerenderReady = false;
    main.properties = [];
    Search.query({ 'search_api_views_fulltext': main._textSearch }).$promise.then(function(result) {
      $rootScope.searching = false;
      main.properties = _.map(result.data, function(property) {
        property.area = findPropertyArea(property);
        return property;
      });
      main.pager.count = result.count;
      updateCategories(result.facets);
      window.prerenderReady = true;
    });
  };

  function findPropertyArea(property) {
    var fields = property.fields;
    if (fields['field_precio_venta_unidad:publish'] == true) {
      var area = {
        unit: null,
        amount: null,
        currency: null
      };
      if (_.isArray(fields['field_precio_venta_unidad:units'])) {
        area.unit = fields['field_precio_venta_unidad:units'][0];
      } else {
        console.log('propiedad con precio de venta por unidad publicado, sin unidad especificada', property);
      }
      if (fields['field_precio_venta_unidad:amount']) {
        area.amount = parseFloat(fields['field_precio_venta_unidad:amount'], 10) || 0;
      } else {
        console.log('propiedad con precio de venta por unidad publicado, sin cantidad especificada', property);
      }
      if (_.isArray(fields['field_precio_venta_unidad:currency'])) {
        area.currency = fields['field_precio_venta_unidad:currency'][0];
      } else {
        console.log('propiedad con precio de venta por unidad publicado, sin modena especificada', property);
      }

      return area;
    } else {
      return null
    }

  };

  var selectPageSize = function(size) {
    main.pager.size = size;
    search(true);
  };

  var selectPage = function(page) {
    main.pager.page = page;
  };

  var setViewMode = function(viewMode) {
    main.viewMode = viewMode;
  };

  var visibleRegion = function() {
    $rootScope.facetAnimate = !$rootScope.facetAnimate;
    if ($rootScope.facetAnimate) {
      $rootScope.searchResults = false;
    } else {
      $rootScope.searchResults = true;
    }
  };

  main.pager = {
    size: 20,
    page: 1
  };

  main.imageSize = 'lg';
  main.viewMode = 'list';
  main.sortList = [
    { name: 'SALES_PRICE_DESC', value: 'field_precio_venta:amount', order: 'DESC', key: 'field_precio_venta:amount_DESC' },
    { name: 'SALES_PRICE_ASC', value: 'field_precio_venta:amount', order: 'ASC', key: 'field_precio_venta:amount_ASC' },
    { name: 'RENTAL_PRICE_DESC', value: 'field_precio_alquiler:amount', order: 'DESC', key: 'field_precio_alquiler:amount_DESC' },
    { name: 'RENTAL_PRICE_ASC', value: 'field_precio_alquiler:amount', order: 'ASC', key: 'field_precio_alquiler:amount_ASC' },
    { name: 'LAND_AREA_DESC', value: 'field_dimension_terreno_m2_sort_', order: 'DESC', key: 'field_dimension_terreno_m2_sort_DESC' },
    { name: 'LAND_AREA_ASC', value: 'field_dimension_terreno_m2_sort_', order: 'ASC', key: 'field_dimension_terreno_m2_sort_ASC' },
    { name: 'BUILDED_AREA_DESC', value: 'field_mconstruidos', order: 'DESC', key: 'field_mconstruidos_DESC' },
    { name: 'BUILDED_AREA_ASC', value: 'field_mconstruidos', order: 'ASC', key: 'field_mconstruidos_ASC' },
    { name: 'ROOMS_DESC', value: 'field_dormitorios_totales', order: 'DESC', key: 'field_dormitorios_totales_DESC' },
    { name: 'ROOMS_ASC', value: 'field_dormitorios_totales', order: 'ASC', key: 'field_dormitorios_totales_ASC' },
    { name: 'NEWER', value: 'created', order: 'DESC', key: 'created_DESC' }
  ];
  main.sort = main.sortList[main.sortList.length - 1];

  main.textSearch = textSearch;
  main.clearFacets = clearFacets;
  main.selectPageSize = selectPageSize;
  main.selectPage = selectPage;
  main.setViewMode = setViewMode;
  main.updateCategories = updateCategories;
  main.headerType = 'small';
  main.search = search;
  main.visibleRegion = visibleRegion;
  main.deleteFilters = function() {
    SearchService.text = '';
    SearchService.location = null;
    SearchService.reset = true;
    $state.go('app.results', {
      lang: $rootScope.lang,
      force: true
    }, {
      reload: true,
      inherit: false,
      notify: true
    });
  }

  screenSize.when('xs', function() {
    if (main.imageSize != 'xs') {
      main.imageSize = 'xs';
    }
    if ($rootScope.facetAnimate === true) {
      $rootScope.facetAnimate = false;
    }
  });
  screenSize.when('sm', function() {
    if (main.imageSize != 'md') {
      main.imageSize = 'md';
    }

    if (main.facetAnimate != true) {
      $rootScope.facetAnimate = true;
    }

    if (main.searchResults != true) {
      $rootScope.searchResults = true;
    }
  });
  screenSize.when('md', function() {

    if (main.imageSize != 'lg') {
      main.imageSize = 'lg';
    }

    if (main.facetAnimate != true) {
      $rootScope.facetAnimate = true;
    }

    if (main.searchResults != true) {
      $rootScope.searchResults = true;
    }
  });
  screenSize.when('lg', function() {
    if (main.imageSize != 'xlg') {
      main.imageSize = 'xlg';
    }

    if (main.facetAnimate != true) {
      $rootScope.facetAnimate = true;
    }

    if (main.searchResults != true) {
      $rootScope.searchResults = true;
    }
  });


  if (screenSize.is('xs')) {
    if (main.facetAnimate != false) {
      $rootScope.facetAnimate = false;
    }
  } 
  else {
    if (main.facetAnimate != true) {
      $rootScope.facetAnimate = true;
    }
  }

  main.goToTop = $rootScope.goToTop;

  function setDefaultCountry(countryName) {
    countryName = countryName.replace(/ /g, "-").toLowerCase();
    var locations = _.find(main.categories, { key: 'field_propiedad_ubicacion' });
    if (!_.isEmpty(locations) && !_.isEmpty(locations.key)) {
      for (var k = 0; k < locations.items.length; k++) {
        var name = locations.items[k].name.replace(/ /g, "-").toLowerCase();
        if (name == countryName) {
          locations.items[k].selected = true;
          break;
        }
      }
    }
  }

  function getMainFiltersFromUrl() {
    var mainFilters = $stateParams.filters.toLowerCase().split("/");
    getLocations(mainFilters);
    getOperations(mainFilters);
    getPropertyTypes(mainFilters);
  }

  function getLocations(mainFilters) {
    var valueTids = [];
    var valueParents = [];
    var locations = _.find(main.categories, { key: 'field_propiedad_ubicacion' });
    if (!_.isEmpty(locations) && !_.isEmpty(locations.key)) {
      for (var i = 0; i < mainFilters.length; i++) {
        var breakCondition = false;
        var mainFiltersTmp = mainFilters[i].split("--");
        for (var j = 0; j < mainFiltersTmp.length; j++) {
          for (var k = 0; k < locations.flatItems.length; k++) {
            var name = locations.flatItems[k].name.replace(/ /g, "-").toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
            var currentParent = locations.flatItems[k].parent;
            if (name == mainFiltersTmp[j]) {
              if (i > 0) {
                loop_children:
                for (var y = valueTids.length; y >= 0 ; y--) {

                  if (angular.isArray(valueTids[y-1])) {
                    for (var x = 0; x < valueTids[y-1].length; x++) {
                      if (currentParent == valueTids[y-1][x]) {
                        breakCondition = true;
                        break loop_children;
                      }
                    }
                  }
                  else {
                    if (currentParent == valueTids[y-1]) {
                      breakCondition = true;
                      break loop_children;
                    }
                  }

                }
              }
              else {
                breakCondition = true;
              }
              if (breakCondition) {
                locations.flatItems[k].selected = true;
                var tidValueParents = valueParents.length - 1;
                if (valueParents[tidValueParents] == locations.flatItems[k].parent) {
                  var tempValueTid = valueTids[tidValueParents];
                  if (angular.isArray(valueTids[tidValueParents])) {
                    valueTids[tidValueParents].push(locations.flatItems[k].tid);
                  }else {
                    valueTids[tidValueParents] = [];
                    valueTids[tidValueParents].push(tempValueTid);
                    valueTids[tidValueParents].push(locations.flatItems[k].tid);
                  }
                }
                else {
                  valueTids.push(locations.flatItems[k].tid);
                }
                valueParents.push(locations.flatItems[k].parent);
                breakCondition = false;
                break;
              }
            }
          }
        }
      }
    }
  }

  function getOperations(mainFilters) {
    var operations = _.find(main.categories, { key: 'field_estado' });
    if (!_.isEmpty(operations) && !_.isEmpty(operations.key)) {
      for (var i = 0; i < mainFilters.length; i++) {
        var mainFiltersTmp = mainFilters[i].split("--");
        for (var j = 0; j < mainFiltersTmp.length; j++) {
          for (var k = 0; k < operations.flatItems.length; k++) {
            var name = operations.flatItems[k].label.replace(/ /g, "-").toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
            if (name == mainFiltersTmp[j]) {
              operations.flatItems[k].selected = true;
              break;
            }
          }
        }
      }
    }
  }

  function getPropertyTypes(mainFilters) {
    var propertyTypes = _.find(main.categories, { key: 'field_propiedad_categoria' });
    if (!_.isEmpty(propertyTypes) && !_.isEmpty(propertyTypes.key)) {
      for (var i = 0; i < mainFilters.length; i++) {
        var mainFiltersTmp = mainFilters[i].split("--");
        for (var j = 0; j < mainFiltersTmp.length; j++) {
          for (var k = 0; k < propertyTypes.flatItems.length; k++) {
            var name = propertyTypes.flatItems[k].name.replace(/ /g, "-").toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
            if (name == mainFiltersTmp[j]) {
              propertyTypes.flatItems[k].selected = true;
              break;
            }
          }
        }
      }
    }
  }

  function updateFiltersUrl() {
    var filtersPath = [];
    filtersPath = setLocationsUrl(filtersPath);
    filtersPath = setOperationsUrl(filtersPath);
    filtersPath = setPropertyTypesUrl(filtersPath);

    if (filtersPath.length) {
      var currentPath = $location.path().split("/");
      var newPath = currentPath[1]+"/"+currentPath[2]+"/"+filtersPath.join("/");
      $location.path(newPath).replace();
    }
  }

  function updateMetaDescriptions() {
    var locationsDescription = getLocationsDescription(); 
    var operationsDescription = getOperationsDescription(); 
    var propertyTypesDescription = getPropertyTypesDescription();
    
    if (propertyTypesDescription == "") {
      propertyTypesDescription = "Propiedades";
      if ($rootScope.lang == "en") {
        propertyTypesDescription = "Properties"
      }
    }

    $rootScope.title = operationsDescription + " " + 
    ($rootScope.lang == "es" ? "de " : "of ") + propertyTypesDescription + " " + 
    ($rootScope.lang == "es" ? "en " : "in ") + locationsDescription + " | Caldeyro Victorica";

    var metaDescription = ($rootScope.lang == "es" ? "Descubre más de " : "Discover more of ") + 
    main.resultText.total + " " + propertyTypesDescription +
    ($rootScope.lang == "es" ? " en " : " for ") + 
    operationsDescription +
    ($rootScope.lang == "es" ? " en " : " in ") + locationsDescription + " | Caldeyro Victorica Bienes Raíces.";

    MetaTags.set("Description", metaDescription, true);
    $rootScope.ogdescription = metaDescription;
  }

  function setLocationsUrl(filtersPath) {
    var locations = _.find(main.categories, { key: 'field_propiedad_ubicacion' });
    if (!_.isEmpty(locations) && !_.isEmpty(locations.key)) {
      if (locations.items && locations.items.length) {
        var filterPath = createFilterUrl(locations.items, 'name');
        if (filterPath != "") {
          filtersPath.push(filterPath);
        }
      }
    }
    return filtersPath;
  }

  function toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }

  function getLocationsDescription() {
    var strUrl = setLocationsUrl([]);
    var description = "";
    if (strUrl.length) {
      var strUrl = strUrl[0].split("/").reverse();
      description = toTitleCase(strUrl.join("/").replace(/--/g, ", ").replace(/-/g, " ").replace(/\//g, ", "));
    }
    return description;
  }

  function getOperationsDescription() {
    var strUrl = setOperationsUrl([]);
    var description = "";
    var and = " y "; 
    if ($rootScope.lang == "en") {
      and = " & "
    }
    if (strUrl.length) {
      description = toTitleCase(strUrl[0].replace(/--/g, and).replace(/-/g, " "));
    }
    else {
      description = [];
      var operations = _.find(main.categories, { key: 'field_estado' });
      if (!_.isEmpty(operations) && !_.isEmpty(operations.key)) {
        for (var k = 0; k < operations.flatItems.length; k++) {
          description.push(toTitleCase(operations.flatItems[k].label));
        }
        description = description.join(and);
      }
    }
    return description;
  }

  function getPropertyTypesDescription() {
    var strUrl = setPropertyTypesUrl([]);
    var description = "";
    if (strUrl.length) {
      description = toTitleCase(strUrl[0].replace(/--/g, ", ").replace(/-/g, " ").replace(/\//g, ", "));
    }
    return description;
  }

  function setOperationsUrl(filtersPath) {
    var operations = _.find(main.categories, { key: 'field_estado' }); 
    if (!_.isEmpty(operations) && !_.isEmpty(operations.key)) {
      if (operations.items && operations.items.length) {
        var filterPath = createFilterUrl(operations.items, 'label');
        if (filterPath != "") {
          filtersPath.push(filterPath);
        }
      }
    }
    return filtersPath;
  }

  function setPropertyTypesUrl(filtersPath) {
    var propertyTypes = _.find(main.categories, { key: 'field_propiedad_categoria' });
    if (!_.isEmpty(propertyTypes) && !_.isEmpty(propertyTypes.key)) {
      if (propertyTypes.items && propertyTypes.items.length) {
        var filterPath = createFilterUrl(propertyTypes.items, 'name');
        if (filterPath != "") {
          filtersPath.push(filterPath);
        }
      }
    }
    return filtersPath;
  }

  function createFilterUrl (items, attr) {
    var brothers = [];
    var children = [];
    for (var i = 0; i < items.length; i++) {
      if (items[i].selected) {
        brothers.push(items[i][attr].replace(/ /g, "-").toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, ""));
        if (items[i].items && items[i].items.length) {
          if (createFilterUrl(items[i].items, attr) != "") {
            children.push(createFilterUrl(items[i].items, attr));
          }
        }
      }
    }

    if (brothers.length) {
      brothers = brothers.join('--');
    }
    if (brothers.length && !children.length) {
      return brothers;
    }
    if (brothers.length > 2) {
      return [brothers, children.join('--')].join('/');
    }
    if (brothers.length == 1) {
      return [brothers, children].join("/")
    }

    return "";
  }

  activate($stateParams.force);
}