'use strict';

/**
 * @ngdoc function
 * @name caldeyroApp.controller:StaticCtrl
 * @description
 * # StaticCtrl
 * Controller of the caldeyroApp
 */


angular.module('caldeyroApp')
  .controller('StaticCtrl', StaticCtrl);

StaticCtrl.$inject = ['$scope', '$rootScope', 'page', 'form', 'menu', 'parent', 'title', '$sce', '$translate'];

function StaticCtrl($scope, $rootScope, page, form, menu, parent, title, $sce, $translate) {
  /*@ngInject*/
  var vm = this;
  vm.lang = $translate.use();
  vm.page = page;

  vm.trustedContent = $sce.trustAsHtml(page.body);
  vm.parent = parent;
  form.title = "request_more_info";
  vm.contactForm = form;
  vm.menu = _.map(menu.menu, function(group) {
    if (group.items && group.items.length > 0) {
      group.open = _.find(group.items, { urlTitle: title }) ? true : false;
    } else {
      group.open = false;
    }

    return group;
  });
  vm.title = page.title;
  vm.urlTitle = title;

  $rootScope.ogkeywords = page.metatag['Keywords'] || '';
  $rootScope.ogdescription = page.metatag['Description'] || '';
  $rootScope.title = page.metatag['Page title'] || '';
  $rootScope.title = $rootScope.title.toUpperCase();
  $rootScope.ogtitle = $rootScope.title;
  $rootScope.ogtype = "webpage";


  $scope.$on('langChanged', function(evt, key) {
    vm.lang = key;
  });

  var t = setTimeout(function() {
    window.prerenderReady = true;
    window.setTimeout(t);
  });

}