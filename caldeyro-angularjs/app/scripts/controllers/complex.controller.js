'use strict';

/**
 * @ngdoc function
 * @name caldeyroApp.controller:PropertycontrollerCtrl
 * @description
 * # PropertycontrollerCtrl
 * Controller of the caldeyroApp
 */

angular.module('caldeyroApp')
  .controller('ComplexCtrl', ComplexCtrl);

ComplexCtrl.$inject = ['$scope', 'Search', '$q', 'screenSize', '$rootScope', 'data', 'form', '$location'];

function ComplexCtrl($scope, Search, $q, screenSize, $rootScope, data, form, $location) {
  /*@ngInject*/
  var vm = this;
  vm.property = data.property;
  vm.related = data.property_related;
  vm.form = form;
  vm.pager = {
    limit: 6,
    page: 1
  };
  vm.lang = $rootScope.lang;

  $scope.$on('langChanged', function() {
    vm.lang = $rootScope.lang;
  });

  console.log('complex ', vm.property);

  if(vm.property && vm.property.fields && vm.property.fields['field_direccion:latitude'] && vm.property.fields['field_direccion:longitude']) {
    vm.direccion_latitude = vm.property.fields['field_direccion:latitude'];
    vm.direccion_longitude = vm.property.fields['field_direccion:longitude'];
  } else if (vm.related.length > 0) {
    vm.direccion_latitude = vm.related[0].fields['field_direccion:latitude'];
    vm.direccion_longitude = vm.related[0].fields['field_direccion:longitude'];
  } else {
    vm.direccion_latitude = 0;
    vm.direccion_longitude = 0;
  }

  $rootScope.ogimage = 'demencia';

  vm.onslideafter = function(slider) {
    vm.imageUrl = slider.element.slides[slider.element.currentSlide].dataset.thumb;
    $rootScope.ogimage = slider.element.slides[slider.element.currentSlide].dataset.thumb;
  }

  $rootScope.ogdescription = vm.property.fields['field_condiciones_generales'].join(' ');
  $rootScope.ogtitle = vm.property.fields.title[0];
  $rootScope.title = vm.property.fields.title[0];
  $rootScope.title = $rootScope.title.toUpperCase();
  vm.url = $location.absUrl();

  var activate = function() {

    if (screenSize.is('xs')) {
      vm.imageSize = 'xs';
    } else if (screenSize.is('sm')) {
      vm.imageSize = 'md';
    } else if (screenSize.is('md')) {
      vm.imageSize = 'lg';
    } else if (screenSize.is('lg')) {
      vm.imageSize = 'xlg';
    }

    buildImages(vm.imageSize);
    window.prerenderReady = true;
  };

  var generateId = function() {
    return Math.ceil(Math.random() * 500) + 500;
  };

  var buildImages = function(size) {
    vm.propertyImages = _.map(vm.property.fields['imageUrl'], function(imgObj) {
      return imgObj[size];
    });
  }

  activate();

  screenSize.when('xs', function() {
    if (vm.imageSize != 'xs') {
      vm.imageSize = 'xs';
      buildImages(vm.imageSize);
    }
  });

  screenSize.when('sm', function() {
    if (vm.imageSize != 'md') {
      vm.imageSize = 'md';
      buildImages(vm.imageSize);
    }
  });

  screenSize.when('md', function() {
    if (vm.imageSize != 'lg') {
      vm.imageSize = 'lg';
      buildImages(vm.imageSize);
    }
  });

  screenSize.when('lg', function() {
    if (vm.imageSize != 'xlg') {
      vm.imageSize = 'xlg';
      buildImages(vm.imageSize);
    }
  });

}