'use strict';
/*jshint latedef: nofunc */
/**
 * @ngdoc function
 * @name caldeyroApp.controller:FacetcategoryCtrl
 * @description
 * # FacetcategoryCtrl
 * Controller of the caldeyroApp
 */
angular.module('caldeyroApp')
  .controller('FacetCategoryController', FacetCategoryController);

FacetCategoryController.$inject = ['$scope', '$rootScope'];

function FacetCategoryController($scope, $rootScope) {
  /*@ngInject*/
  var vm = this;
  vm.lang = $rootScope.lang;
  $scope.$on('langChanged', function(evt, key) {
    vm.lang = key;
  });

  vm.value = null;
  vm.selectedValue = null;
  var toggleLimit = function() {
    if (vm.limit === 4) {
      vm.limit = vm.category.items.length;
      vm.limited = false;
    } else {
      vm.limit = 4;
      vm.limited = true;
    }
  };

  var setCategoryValue = function(itemSingle) {
    angular.forEach(vm.category.items, function(item) {
      item.selected = false;
    });
    if (itemSingle.value) {
      itemSingle.selected = true;
    } else {
      vm.category.clear();
    }
    vm.selectedValue = itemSingle.label;
    itemSelected();
  };

  var getLabel = function(lang, delimiter) {
    return vm.category.getLabels(lang, delimiter);
  };

  var itemSelected = function() {
    $scope.$emit('reloadSearch', { value: vm.value });
  };

  vm.getSelectedLabel = function() {
    angular.forEach(vm.category.items, function(item) {
      if (item.selected == true) vm.selectedValue = item.label;
    });
    return vm.selectedValue;
  };

  vm.prevselected = null;

  $scope.$on('facetSelected', function(target, args) {
    if (vm.category.settings.operator === 'and') {

      angular.forEach(vm.category.items, function(item) {
        if(item.selected == true)  vm.prevselected = item;
        item.notify = false;
        item.selected = false;
      });
      
      if(vm.prevselected == args.item) {
        args.item.selected = false;
      } else {
        args.item.selected = true;
      }
    }
    $scope.$emit('reloadSearch', args);
  });
  vm.filtered = [];
  vm.itemSelected = itemSelected;
  vm.setCategoryValue = setCategoryValue;
  vm.getLabel = getLabel;
  vm.caretHTML = '<span class="caret"></span>';

  vm.toggleLimit = toggleLimit;
  vm.limit = 4;
  vm.limited = true;

  vm.positiveValues = function(flag) {
    var l = _.filter(vm.category.items, function(item) {
      var count = false;
      if (flag) {
        count = item.count > 0;
      } else {
        count = item.counts > 0;
      }
      return count;
    }).length;

    return l;
  }


  vm.greaterThan = function(prop, val) {
    return function(item) {
      return item[prop] > val;
    }
  }
}