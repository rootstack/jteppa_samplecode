'use strict';
/*jshint latedef: nofunc */

/**
 * @ngdoc function
 * @name caldeyroApp.controller:FacetitemcontrollerCtrl
 * @description
 * # FacetitemcontrollerCtrl
 * Controller of the caldeyroApp
 */
angular.module('caldeyroApp')
  .controller('FacetItemController', FacetItemController);

FacetItemController.$inject = ['Facets', '$scope'];

function FacetItemController(Facets, $scope) {
  /*@ngInject*/
  var vm = this;
  vm.item.notify = true;
  vm.lang = 'es';
  $scope.$on('langChanged', function(evt, key) {
    vm.lang = key;
  });

  var toggleLimit = function() {
    if (vm.limit === 4) {
      vm.limit = vm.item.items.length;
      vm.limited = false;
    } else {
      vm.limit = 4;
      vm.limited = true;
    }
  };

  var listenerSelectedValue = function(newVal, oldVal) {
    if (newVal !== oldVal && vm.item.notify) {
      //$scope.$emit('facetSelected', { value: newVal, item: vm.item });
    } else {
      if (newVal !== oldVal && !vm.item.notify) {
        vm.item.notify = true;
      }
    }
  };

  vm.positiveValues = function() {
    var l = _.filter(vm.item.items, function(item) {
      return item.counts > 0;
    }).length;
    return l;
  }

  vm.greaterThan = function(prop, val) {
    return function(item) {
      return item[prop] > val;
    }
  }

  vm.toggleValue = function(skip) {
    
    if(!skip) {
      vm.item.selected = !vm.item.selected;
      vm.item.notify = true;
    }
    
    Facets.setLastSelected(vm.item);
    $scope.$emit('facetSelected', { value: vm.item.selected, item: vm.item });
  }

  vm.select = function() {
    vm.item.selected = !vm.item.selected;
    Facets.setLastSelected(vm.item);
    $scope.$emit('facetSelected', { value: vm.item.selected, item: vm.item });
  }

  vm.toggleLimit = toggleLimit;
  vm.limit = 4;
  vm.limited = true;
}