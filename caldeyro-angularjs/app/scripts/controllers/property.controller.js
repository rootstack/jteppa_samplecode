'use strict';

/**
 * @ngdoc function
 * @name caldeyroApp.controller:PropertycontrollerCtrl
 * @description
 * # PropertycontrollerCtrl
 * Controller of the caldeyroApp
 */

angular.module('caldeyroApp')
  .controller('PropertyCtrl', PropertyCtrl);

PropertyCtrl.$inject = ['$scope', 'Search', '$q', 'screenSize', '$rootScope', 'property', 'form', '$location', 'MostViewed', '$timeout'];

function PropertyCtrl($scope, Search, $q, screenSize, $rootScope, property, form, $location, MostViewed, $timeout) {
  /*@ngInject*/
  var vm = this;
  vm.property = property;

  vm.form = form;
  vm.pager = {
    limit: 6,
    page: 1
  };
  vm.lang = $rootScope.lang;

  $rootScope.ogurl = $location.absUrl();

  $scope.$on('langChanged', function() {
    vm.lang = $rootScope.lang;
  });

  vm.printWatery = function() {
    if (vm.property.fields.field_tipo_aguadas) {
      return vm.property.fields.field_tipo_aguadas.indexOf("Naturales") >= 0 || vm.property.fields.field_tipo_aguadas.indexOf("Naturals") >= 0;
    } else {
      return false;
    }
  }

  vm.showMap = vm.property.fields['field_direccion:latitude'] &&
    vm.property.fields['field_direccion:longitude'];

  vm.getDevelopmentLevelText = function() {
    var fields = vm.property.fields;
    var text = '';
    if (fields.field_nivel_de_desarrollo == 'Construido') {

      var construidoEn = fields['field_construido:field_construido_en'] && fields['field_construido:field_construido_en'][0] ? fields['field_construido:field_construido_en'][0] : null;
      var reformaEn = fields['field_construido:field_reforma_en'] && fields['field_construido:field_reforma_en'][0] ? fields['field_construido:field_reforma_en'][0] : null;

      if (construidoEn) {
        text += 'Construido en ' + construidoEn + '. ';
      }

      if (reformaEn) {
        text += 'Con reforma en ' + reformaEn + '. ';
      }

      if (fields.field_estado_de_la_construccion) {
        text += 'Estado ' + fields.field_estado_de_la_construccion;
      }
    } else if (fields.field_nivel_de_desarrollo == 'En construccion') {
      text += fields.field_nivel_de_desarrollo + '. ';
      if (fields.field_mes_de_entrega || fields.field_ano_de_entrega) {
        text += 'Con entrega en ' + (fields.field_mes_de_entrega || '') + ' ' + (fields.field_ano_de_entrega || '') + '. ';
      }
    } else if (fields.field_nivel_de_desarrollo == 'Proyecto') {
      if (fields.field_proyecto) {
        text += fields.field_proyecto + '. ';
      } else {
        text += fields.field_nivel_de_desarrollo + '. ';
      }

      if (fields.field_mes_de_entrega || fields.field_ano_de_entrega) {
        text += 'Entrega en ' + (fields.field_mes_de_entrega || '') + ' ' + (fields.field_ano_de_entrega || '') + '.';
      }
    }
    return text;
  }
  vm.onslideafter = function(slider) {
    vm.imageUrl = slider
      .element
      .slides[slider.element.currentSlide]
      .dataset
      .thumb;

    $rootScope.ogimage = slider
      .element
      .slides[slider.element.currentSlide]
      .dataset
      .thumb;
    vm.showGalley = true;
  }

  vm.url = $location.absUrl();

  vm.flexsliderAsNav = function(flexslider) {

  }

  var findPropertyArea = function(property) {
    var fields = property.fields;

    if (+fields['field_precio_venta_unidad:publish'] == 1) {
      var area = {
        unit: null,
        amount: null,
        currency: null
      };

      area.unit = _.get(fields, '[field_precio_venta_unidad:units][0]', '');

      if (area.unit == "has" || area.unit == "hás" || area.unit == "has." || area.unit == "hás.") {
        area.unit = "ha";
      }
      area.amount = parseFloat(fields['field_precio_venta_unidad:amount'], 10) || 0;
      area.currency = _.get(fields,'[field_precio_venta_unidad:currency][0]', '');

      return area;
    }
    else {
      return null
    }

  };
  var activate = function() {
    vm.nextPage = function() {
      if (((vm.pager.page + 1) * vm.pager.limit) > vm.mostViewed.length) {
        vm.pager.page = 1;
      } else {
        vm.pager++;
      }
    };

    vm.prevPage = function() {
      if (vm.pager.page === 1) {
        vm.pager.page = Math.ceil(vm.mostViewed.length / vm.pager.limit);
      } else {
        vm.pager--;
      }
    };

    vm.mostViewed = [];
    var category_id = vm.property.fields['field_propiedad_categoria'];
    if (_.isArray(category_id)) category_id = category_id[0];

    if (screenSize.is('xs')) {
      vm.imageSize = 'xs';
    } else if (screenSize.is('sm')) {
      vm.imageSize = 'md';
    } else if (screenSize.is('md')) {
      vm.imageSize = 'lg';
    } else if (screenSize.is('lg')) {
      vm.imageSize = 'xlg';
    }

    buildImages(vm.imageSize);

    // Format property fields.
    vm.property.area = findPropertyArea(vm.property);

    return MostViewed.query(false, vm.property.id, category_id, $rootScope.lang).then(function(response) {
      vm.mostViewed = response;
      var tmp = 'md';
      if (screenSize.is('xs')) {
        tmp = 'xs';
      } else if (screenSize.is('sm')) {
        tmp = 'md';
      } else if (screenSize.is('md')) {
        tmp = 'lg';
      } else if (screenSize.is('lg')) {
        tmp = 'xlg';
      }

      return buildMostViewedImages(tmp);

    }, function(response) {
      return true;
    });


  };

  var generateId = function() {
    return Math.ceil(Math.random() * 500) + 500;
  };
  var imageSizes = ['xs', 'md', 'lg', 'xlg'];
  var buildImages = function(size) {
    var ix = imageSizes.indexOf(size);
    vm.propertyImages = _.map(vm.property.fields['imageUrl'], function(imgObj) {
      if (!_.isUndefined(imgObj[size])) {
        return imgObj[size];
      }
      var fallback = null;
      _.each(imgObj, function(url, key) {
        if (imageSizes.indexOf(key) >= ix) {
          fallback = url;
          return false;
        }
      });
      return fallback;
    });
  };

  var buildMostViewedImages = function(size) {
    vm.mostViewed = _.map(vm.mostViewed, function(property) {
      property.images = _.map(property.image_url, function(imgObj) {
        return imgObj[size];
      });
      return property;
    });
    window.prerenderReady = true;
  }

  activate();

  screenSize.when('xs', function() {
    vm.imageSize = 'xs';
    buildImages(vm.imageSize);
    buildMostViewedImages(vm.imageSize);
  });

  screenSize.when('sm', function() {
    vm.imageSize = 'md';
    buildImages(vm.imageSize);
    buildMostViewedImages(vm.imageSize);
  });

  screenSize.when('md', function() {
    vm.imageSize = 'lg';
    buildImages(vm.imageSize);
    buildMostViewedImages(vm.imageSize);
  });

  screenSize.when('lg', function() {
    vm.imageSize = 'xlg';
    buildImages(vm.imageSize);
    buildMostViewedImages(vm.imageSize);
  });



}