'use strict';
/*jshint latedef: nofunc */
/**
 * @ngdoc service
 * @name caldeyroApp.search
 * @description
 * # search
 * Service in the caldeyroApp.
 */
angular.module('caldeyroApp')
  .service('Search', SearchResource);

SearchResource.$inject = ['$resource', 'globalConstant', '$http', '$q', '$state', '$translate'];

function SearchResource($resource, globalConstant, $http, $q, $state, $translate) {
  /*@ngInject*/
  var prevRequest;
  var Search = function() {

    return $resource('https://api.caldeyro.com/services/caldeyro_search_api/contenido', {}, {
      //var Search = $resource(globalConstant.mockUrl+'contenido', { }, {
      query: {
        method: 'GET',
        isArray: false,
        cancellable: true,
        transformResponse: function(data) {

          if (data) {
            var wrapped = angular.fromJson(data);

            angular.forEach(wrapped.data, function(item, index) {
              wrapped.data[index].location = (wrapped
                  .data[index]
                  .fields['field_propiedad_ubicacion:name'] || [])
                .reverse()
                .join(', ');

              var coneat = parseInt(wrapped.data[index].fields.field_indice_prod_promedio, 10);
              if (_.isNumber(coneat) && coneat > 0) {
                wrapped.data[index].fields.field_indice_prod_promedio = coneat;
              } else {
                wrapped.data[index].fields.field_indice_prod_promedio = null;
              }

              var title = wrapped.data[index].fields.field_node_link;

              wrapped.data[index].parsedTitle = title;

            });

            return wrapped;
          } else {
            return { data: [] };
          }

        }
      }
    });
  }

  var results = { 'es': { data: [] }, 'en': { data: [] } };

  var _SearchService = {
    query: function(force, params) {

      var deferred = $q.defer();

      if (prevRequest) {
        prevRequest.$cancelRequest();
      }
      prevRequest = Search().query(params);
      prevRequest.$promise.then(function(data) {
        deferred.resolve(data);
      }, function() {
        deferred.reject();
      });

      return deferred.promise;
    }
  };

  return _SearchService;
}