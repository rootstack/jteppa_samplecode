'use strict';

/**
 * @ngdoc service
 * @name caldeyroApp.staticPageParser
 * @description
 * # staticPageParser
 * Service in the caldeyroApp.
 */
angular.module('caldeyroApp')
  .service('staticPageParser', function () {
    /*@ngInject*/
  	function parse(data) {
      if(data && data.body && data.body.safe_value) {
        var html = data.body.safe_value;
        data.body = html.replace('/sites/default/files/', 'https://www.caldeyro.com/sites/default/files/');
      }
  		
  		return data;
  	}

  	var parser = {
  		parse : parse
  	};

    return parser;
  });
