'use strict';

/**
 * @ngdoc service
 * @name caldeyroApp.metaTags
 * @description
 * # metaTags
 * Service in the caldeyroApp.
 */
 angular.module('caldeyroApp')
 .service('MetaTags', MetaTags);

 MetaTags.$inject = ['$resource', 'globalConstant', '$http', '$q', '$rootScope'];

 function MetaTags ($resource, globalConstant, $http, $q, $rootScope) {
  /*@ngInject*/
  var metaTags = [];

  function slugify(text) {
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
  }

  var MenuService = {
   getAll: function () {
     return metaTags;
   },
   get : function (key) {
    var id = slugify(key);
    return metaTags[id];
   },
   set : function (key, metaTag, notify) {
    var id = slugify(key);
    metaTags[id] = {name: id, content: metaTag};
    if(notify) {
      $rootScope.$emit('meta-tags-updated');
    }
  },
  setAll: function (metaTags) {
    var that = this;
    if(angular.isObject(metaTags)) {
      angular.forEach(metaTags, function(value, key) {
        that.set(key, metaTags[key], false);
      });
      $rootScope.$emit('meta-tags-updated');
    }
  }

};

return MenuService;
}
