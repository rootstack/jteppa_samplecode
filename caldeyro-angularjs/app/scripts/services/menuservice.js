'use strict';

/**
 * @ngdoc service
 * @name caldeyroApp.MenuService
 * @description
 * # MenuService
 * Service in the caldeyroApp.
 */

angular.module('caldeyroApp')
  .service('MenuService', MenuService);

MenuService.$inject = ['$resource', 'globalConstant', '$http', '$q', '$rootScope', '$translate'];

function MenuService($resource, globalConstant, $http, $q, $rootScope, $translate) {

  /*@ngInject*/
  function buildItem(raw) {
    var tmp = {};
    tmp.title = raw.link.link_title;
    tmp.urlTitle = raw.link.link_path.replace('informacion/', '').replace('information/', '');
    tmp.link_path = raw.link.link_path;
    if (Object.prototype.toString.call(raw.below) === '[object Object]' && Object.keys(raw.below).length > 0) {
      tmp.items = mapItems(raw.below);
    } else {
      if (raw.link.link_path) {
        tmp.id = raw.link.link_path;
      }
    }
    return tmp;
  }

  function mapItems(items) {
    var _items = [];
    for (var key in items) {
      if (items.hasOwnProperty(key)) {
        _items.push(buildItem(items[key]));
      }
    }
    return _items;
  }

  var MenuResource = $resource('https://api.caldeyro.com/es/services/caldeyro_menu/:lang/', {}, {
    query: {
      method: 'GET',
      isArray: false,
      transformResponse: function(data) {
        var menu = { menu: [] };
        var wrapped = angular.fromJson(data);
        for (var key in wrapped) {
          if (wrapped.hasOwnProperty(key)) {
            menu.menu.push(buildItem(wrapped[key]));
          }
        }
        return menu;
      }
    }
  });

  var menu = { 'es': [], 'en': [] };
  var translateddUrl = '';
  var _MenuService = {
    setTranslatedUrl: function(url) {
      translateddUrl = url;
    },
    goToTranslatedUrl : function() {
      window.location = translateddUrl;
    },
    query: function(force, type, lang) {
      lang = _.isUndefined(lang) ? $translate.use() : lang;
      var deferred = $q.defer();
      if (_.isUndefined(menu[type])) {
        MenuResource.query({ lang: lang, menu_name: type }).$promise.then(function(data) {
          menu[lang][type] = data;
          deferred.resolve(menu[lang][type]);
        }, function() {
          deferred.reject();
        });
      } else {
        deferred.resolve(menu[lang][type]);
      }
      return deferred.promise;
    }
  };

  return _MenuService;
}