'use strict';
/*jshint latedef: nofunc */
/**
 * @ngdoc service
 * @name caldeyroApp.search
 * @description
 * # search
 * Service in the caldeyroApp.
 */
 angular.module('caldeyroApp')
 .service('MostViewed', MostViewed);

 MostViewed.$inject = ['$resource', 'globalConstant', '$http', '$q'];

 function MostViewed ($resource, globalConstant, $http, $q) {
  /*@ngInject*/
  var MostViewedResource = $resource('https://api.caldeyro.com/services/caldeyro_search_api/contenido', { }, {
  //var Search = $resource(globalConstant.mockUrl+'contenido', { }, {
    query: {
      method: 'GET',
      isArray: false,
      transformResponse: function(data) {
        var wrapped = angular.fromJson(data);
        return wrapped;
      }
    }
  });

  var results = {};

  var MostViewedService = {
    query : function(force, nid, category) {
      var key = nid+''+category;
      var deferred = $q.defer();
      if(force || results[key].length === 0) {
        MostViewedResource.query({nid:nid, category_property: category}).$promise.then(function(data){
          results[key] = data;
          deferred.resolve(results);
        }, function() {
          deferred.reject();
        });
      } else {
        deferred.resolve(results);
      }
      return deferred.promise;
    }
  };

  return MostViewedService;
}
