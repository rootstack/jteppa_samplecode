'use strict';
/*jshint latedef: nofunc */

_.mixin({
  'findByValues': function(collection, property, values) {
    return _.filter(collection, function(item) {
      return !_.includes(values, item[property]);
    });
  }
});

 var selectedItem;

var Category = function(atts) {
  var self = this;
  var initialSettings = atts || {};
  //initial settings if passed in
  for (var setting in initialSettings) {
    if (initialSettings.hasOwnProperty(setting)) {
      self[setting] = initialSettings[setting];
    }
  }

  self.getParams = function() {
    var param = {};
    param[self.key] = self.getValues();
    return param;
  };

  self.getValues = function() {
    var values = [];
    angular.forEach(self.items, function(item) {
      values = values.concat(item.getValue());
    });
    values = _.reject(values, _.isNull);
    if (self.widget === 'facetapi_links') {
      values = values.map(function(value) {
        return value.filter;
      });
    } else if (self.widget === 'facetapi_checkbox_links') {
      values = _.map(values, function(value) {
        return value.tid || value.filter;
      });
    } else if (self.widget === 'search_api_ranges_ui_select') {
      values = _.map(values, function(value) {
        return value.value;
      });
    }
    return values;
  };

  self.getActiveNames = function(delimiter) {
    var labels = [];
    var _tmp = null;
    angular.forEach(self.items, function(item) {
      if (self.key == 'field_estado' || self.key == 'field_vistas' || self.key == 'field_caract_grales_exteriores' || self.key == 'field_aptitud_o_potencial') {
        _tmp = item.getActiveSingleName();
      } else {
        _tmp = item.getActiveNames();
      }
      if (_tmp) {
        labels = labels.concat(_tmp);
      }
    });
    labels = labels.join(delimiter);
    return labels;
  };

  self.getLabels = function(lang, delimiter) {
    var labels = [];
    var _tmp = null;
    angular.forEach(self.items, function(item) {
      _tmp = item.getLabel(lang);
      if (_tmp) {
        labels = labels.concat(_tmp);
      }
    });
    labels = labels.join(delimiter);
    return labels;
  };

  self.clear = function() {
    angular.forEach(self.items, function(item) {
      item.clear();
    });
  };

  self.untouch = function() {
    angular.forEach(self.items, function(item) {
      item.untouch();
    });
  }

  self.cleanUntouched = function(items) {
    angular.forEach(items, function(item) {
      if (item.touched != true) {
        item.hide();
      } else {
        if (item.items) {
          self.cleanUntouched(item.items);
        }
      }
    });
  }

  function deepUpdate(items, key, targetKey, count) {
    
    for (var i = 0; i < items.length; i++) {
      if (items[i][key] == targetKey) {
        items[i].touched = true;
        items[i].counts = count;
        items[i].count = count;
        items[i].value = count;
      } 

      if ( _.isArray(items[i].items)) {
        deepUpdate(items[i].items, key, targetKey, count);
      }
    }

    for (var i = 0; i < items.length; i++) {
      if(items[i].touched == false) {
          if ((self.key === 'field_propiedad_categoria' || self.key === 'field_propiedad_ubicacion') && ((selectedItem && selectedItem.key !== self.key) || (typeof selectedItem === 'undefined'))) {
            items[i].hide();
          } else if(!(self.key === 'field_propiedad_categoria' || self.key === 'field_propiedad_ubicacion') ) {
            items[i].hide();
          }
      }
    }
  }

  self.sort = function() {
    // var property = 'count';
    // if (self.key === 'field_propiedad_categoria' || self.key === 'field_propiedad_ubicacion') {
    //   property = 'counts';
    // }
    self.items = self.items.sort(function(a, b) {
      var o1 = _.isUndefined(a.selected) ? false : a.selected;
      var o2 = _.isUndefined(b.selected) ? false : b.selected;

      var p1 = a.count;
      var p2 = b.count;
      if (o1 > o2) return -1;
      if (o1 < o2) return 1;
      if (p1 > p2) return -1;
      if (p1 < p2) return 1;
      return 0;
    });
    _.forEach(self.items, function(item) {
      item.sortChildren();
    });
  };

  self.update = function(items) {
    var i = 0;
    var j = 0;
    var tmp = {};
    var founded = [];
    if (self.key === 'field_propiedad_categoria' || self.key === 'field_propiedad_ubicacion') {
      self.untouch();
      for (i = 0; i < items.length; i++) {
        deepUpdate(self.items, 'tid', items[i].filter, items[i].count);
      }

      self.cleanUntouched(self.items);
    } else if (self.widget == 'search_api_ranges_ui_select') {
      self.untouch();
      if (_.isObject(items)) {
        var itemsArray = [];
        for (var i in items) {
          if (items.hasOwnProperty(i)) {
            itemsArray.push(items[i]);
          }
        }
        items = itemsArray;
      }
      for (i = 0; i < items.length; i++) {
        tmp = _.find(self.items, { label: items[i].label });
        if (tmp) {
          tmp.count = items[i].count;
          founded.push(tmp.label);
        }
      }
      _.forEach(_.findByValues(self.items, 'label', founded), function(item) {
        item.count = 0;
      });
      
    } else {
      self.untouch();
      if (_.isObject(items)) {
        var itemsArray = [];
        for (var i in items) {
          if (items.hasOwnProperty(i)) {
            itemsArray.push(items[i]);
          }
        }
        items = itemsArray;
      }
      for (i = 0; i < items.length; i++) {
        tmp = _.find(self.items, { filter: items[i].filter });
        if (tmp) {
          tmp.count = items[i].count;
          founded.push(tmp.filter);
        }
      }
      _.forEach(_.findByValues(self.items, 'filter', founded), function(item) {
        item.count = 0;
      });
    }
    self.sort();
  };

  return self;
};

/*jshint latedef: nofunc */
var Item = function(atts) {
  var self = this;
  var initialSettings = atts || {};
  //initial settings if passed in
  for (var setting in initialSettings) {
    if (initialSettings.hasOwnProperty(setting)) {
      self[setting] = initialSettings[setting];
      self.selected = false;
    }
  }

  var _mark = function(item) {
    item.selected = true;
    if (!_.isEmpty(item.parentItem)) {
      _mark(item.parentItem);
    }
  }
  self.mark = function() {
    _mark(self);
  }
  self.getSelectedItems = function() {
    var value = null;
    if (self.hasValue()) {
      angular.forEach(self.items, function(item) {

        var _tmpValue = item.getSelectedItems();

        if (_tmpValue && _tmpValue.length > 0) {
          _tmpValue = _tmpValue.filter(function(e) { return e; });
        }

        if (angular.isArray(_tmpValue)) {
          if (!angular.isArray(value)) {
            value = [];
          }
          value = value.concat(_tmpValue);
        }
      });

      if (angular.isArray(value)) {
        value = value.filter(function(e) { return e; });
      }

      if (!angular.isArray(value)) {
        value = [self];
      }

    }
    return value;
  };

  self.sortChildren = function() {
    self.items = self._sortChildren();
  }

  self._sortChildren = function() {
    if (self.items && _.isArray(self.items)) {
      self.items = self.items.sort(function(a, b) {
        var o1 = _.isUndefined(a.selected) ? false : a.selected;
        var o2 = _.isUndefined(b.selected) ? false : b.selected;

        var p1 = a.counts;
        var p2 = b.counts;
        if (o1 > o2) return -1;
        if (o1 < o2) return 1;
        if (p1 > p2) return -1;
        if (p1 < p2) return 1;
        return 0;
      });
      _.forEach(self.items, function(item) {
        item.sortChildren();
      });
    }

    return self.items;
  }

  self.getSelectedItemsTree = function() {
    var value = null;
    if (self.hasValue()) {
      angular.forEach(self.items, function(item) {

        var _tmpValue = item.getSelectedItems();

        if (_tmpValue && _tmpValue.length > 0) {
          _tmpValue = _tmpValue.filter(function(e) { return e; });
        }

        if (angular.isArray(_tmpValue)) {
          if (!angular.isArray(value)) {
            value = [self];
          }
          value = value.concat(_tmpValue);
        }
      });

      if (angular.isArray(value)) {
        value = value.filter(function(e) { return e; });
      }

      if (!angular.isArray(value)) {
        value = [self];
      }
    }
    return value;
  };

  self.getValue = function() {
    var value = null;
    var selectedItems = self.getSelectedItems();

    if (angular.isArray(selectedItems) && selectedItems.length > 0) {
      value = selectedItems;
    }
    return value;
  };

  self.getLabel = function(lang) {
    var label = null;
    var selectedItems = self.getSelectedItems();
    if (angular.isArray(selectedItems) && selectedItems.length > 0) {
      label = selectedItems.map(function(item) {
        return item.label[lang];
      });
    }
    return label;
  };

  self.getActiveSingleName = function() {
    var name = null;
    var selectedItems = self.getSelectedItemsTree();
    if (angular.isArray(selectedItems) && selectedItems.length > 0) {
      name = selectedItems.map(function(item) {
        return item.filter;
      });
    }
    return name;
  };

  self.getActiveNames = function() {
    var name = null;
    var selectedItems = self.getSelectedItemsTree();
    if (angular.isArray(selectedItems) && selectedItems.length > 0) {
      name = selectedItems.map(function(item) {
        return item.name;
      });
    }
    return name;
  };

  self.hasValue = function() {
    return self.selected ? true : false;
  };

  self.clear = function() {
    self.notify = false;
    self.selected = false;
    if (hasChildren()) {
      angular.forEach(self.items, function(item) {
        item.clear();
      });
    }
  };

  self.untouch = function() {
    self.touched = false;
    self.show();
    if (hasChildren()) {
      angular.forEach(self.items, function(item) {
        item.untouch();
      });
    }
  }

  self.hide = function() {
    self._value = self.value || self.count || self.counts ;
    self.value = 0;
    self.count = 0;
    self.counts = 0;
     
  };

  self.show = function() {
     self.touched = false;
  }

  function hasChildren() {
    return self.hasChildren &&
      angular.isArray(self.items) &&
      self.items.length > 0;
  }

  return self;
};

function cleanItems(items, parent, root) {
  var _items = [];
  var newItem = null;
  for (var item in items) {
    if (items.hasOwnProperty(item)) {
      newItem = new Item(items[item]);
      newItem.count = newItem.counts ? newItem.counts : newItem.count;
      newItem.count = newItem.count ? newItem.count : 0;
      newItem.counts = newItem.count;
      newItem.key = root.key;
      if (newItem.children) {
        newItem.hasChildren = true;
        newItem.selected = false;
        newItem.items = cleanItems(newItem.children, newItem, root);
      }

      _items.push(newItem);
      if (!_.isEmpty(parent)) {
        newItem.parentItem = parent;
      }
    }
  }

  _items = _items.sort(function(a, b) {
    var o1 = _.isUndefined(a.selected) ? false : a.selected;
    var o2 = _.isUndefined(b.selected) ? false : b.selected;

    var p1 = a.count;
    var p2 = b.count;
    if (o1 > o2) return -1;
    if (o1 < o2) return 1;
    if (p1 > p2) return -1;
    if (p1 < p2) return 1;
    return 0;
  });
  root.flatItems = _.concat(root.flatItems, _items);
  return _items;
}
/**
 * @ngdoc service
 * @name caldeyroApp.facets
 * @description
 * # facets
 * Factory in the caldeyroApp.
 */
angular.module('caldeyroApp')
  .factory('Facets', FacetResource);

FacetResource.$inject = ['$resource', 'globalConstant', '$http', '$q'];

function FacetResource($resource, globalConstant, $http, $q) {
  /*@ngInject*/
  $http.defaults.useXDomain = true;
  var _FacetResource = $resource('https://api.caldeyro.com/es/services/caldeyro_facets_search_api/contenido', {}, {
    //var FacetResource = $resource(globalConstant.mockUrl+'facets', { }, {
    queryFacets: {
      method: 'GET',
      isArray: false,
      transformResponse: function(data) {

        if (data) {
          var wrapped = angular.fromJson(data);
          var facets = [];
          var items = [];

          for (var i in wrapped.facets) {
            if (wrapped.facets.hasOwnProperty(i)) {
              facets.push(wrapped.facets[i]);
            }
          }
          wrapped.categories = facets;

          angular.forEach(wrapped.categories, function(category, index) {
            wrapped.categories[index] = new Category(category);
            category = wrapped.categories[index];
            items = [];
            category.flatItems = [];
            category.items = cleanItems(category.items, null, category);
          });
          return wrapped;
        }
      }
    }
  });
  
  var facets = { categories: [] };
 
  var FacetService = {
    setLastSelected : function(item) {
      selectedItem = item;
    },
    getLastSelected :  function() {
      return selectedItem;
    },
    getAll: function(force, params) {
      var deferred = $q.defer();
      if (force || facets.categories.length === 0) {
        _FacetResource.queryFacets(params).$promise.then(function(data) {
          facets = data;
          deferred.resolve(data);
        }, function() {
          deferred.reject();
        });
      } else {
        deferred.resolve(facets);
      }
      return deferred.promise;
    }
  };

  return FacetService;
}