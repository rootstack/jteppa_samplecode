'use strict';

/**
 * @ngdoc service
 * @name caldeyroApp.form
 * @description
 * # form
 * Service in the caldeyroApp.
 */
angular.module('caldeyroApp')
  .service('Form', FormService);


FormService.$inject = ['$resource', 'globalConstant', '$http', '$q'];

function FormService($resource, globalConstant, $http, $q) {
  /*@ngInject*/
  var Form = $resource('https://api.caldeyro.com/services/caldeyro_form_api/:lang?key=:key', {}, {
    //var Search = $resource(globalConstant.mockUrl+'contenido', { }, {
    get: {
      method: 'GET',
      isArray: false,
      transformResponse: function(data) {
        var wrapped = angular.fromJson(data);
        angular.forEach(wrapped.field, function(field, key) {
          if (field.type === 'select') {
            field.extra.items = _.omit(field.extra.items, [""]);
            if (field.form_key === 'pais' || field.form_key === 'country') {
              field.value = 'UY';
            } else {
              var keys = _.keys(field.extra.items);
              if (keys.length > 0) {
                field.value = keys[0];
              }
            }
          }
        });
        return wrapped;
      }
    }
  });

  var formData = { 'es': {}, 'en': {} };

  var _FormService = {
    get: function(force, lang, key) {
      var deferred = $q.defer();
      if (force || !formData[lang][key]) {
        Form.get({ lang: lang, key: key }).$promise.then(function(data) {
          formData[lang][key] = data;
          deferred.resolve(data);
        }, function() {
          deferred.reject();
        });
      } else {
        deferred.resolve(formData[lang][key]);
      }
      return deferred.promise;
    }
  };

  return _FormService;
}