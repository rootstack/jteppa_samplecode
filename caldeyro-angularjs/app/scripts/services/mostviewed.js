'use strict';
/*jshint latedef: nofunc */
/**
 * @ngdoc service
 * @name caldeyroApp.search
 * @description
 * # search
 * Service in the caldeyroApp.
 */
 angular.module('caldeyroApp')
 .service('MostViewed', MostViewed);

 MostViewed.$inject = ['$resource', 'globalConstant', '$http', '$q'];

 function MostViewed ($resource, globalConstant, $http, $q) {
  /*@ngInject*/
  var MostViewedResource = $resource('https://api.caldeyro.com/es/services/caldeyro_most_viewed_properties/:lang', { }, {
  //var Search = $resource(globalConstant.mockUrl+'contenido', { }, {
    query: {
      method: 'GET',
      isArray: true,
      transformResponse: function(data) {
        var wrapped = angular.fromJson(data);
        return wrapped;
      }
    }
  });

  var results = {};

  var MostViewedService = {
    query : function(force, nid, category, lang) {
      var key = nid+''+category+lang;
      var params = {nid:nid, category_property: category, lang: lang};
      var deferred = $q.defer();
      if(force || _.isUndefined(results[key]) || results[key].length === 0) {
        MostViewedResource.query(params).$promise.then(function(data){
          results[key] = data;
          deferred.resolve(data);
        }, function() {
          deferred.reject();
        });
      } else {
        deferred.resolve(results[key]);
      }
      return deferred.promise;
    }
  };

  return MostViewedService;
}
