'use strict';

/**
 * @ngdoc service
 * @name caldeyroApp.globalConstant
 * @description
 * # globalConstant
 * Constant in the caldeyroApp.
 */
angular.module('caldeyroApp')
  .constant('globalConstant', {
  	serverUrl: 'https://api.caldeyro.com/drupalgap/caldeyro_search_api/',
  	mockUrl: 'http://127.0.0.1:3333/'
  });
