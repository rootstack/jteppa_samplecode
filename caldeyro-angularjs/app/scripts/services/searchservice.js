'use strict';

/**
 * @ngdoc service
 * @name caldeyroApp.searchService
 * @description
 * # searchService
 * Service in the caldeyroApp.
 */
angular.module('caldeyroApp')
  .service('SearchService', function () {
    /*@ngInject*/
    var SearchService = {
      text: '',
      location: null,
      reset: false,
      ref :  false,
      setText : function(text) {
        var refRegEx = /\d{4}-[A-Za-z]{2}/g;
        SearchService.text = text;
        SearchService.ref = refRegEx.test(text);
      }
    };
    return SearchService;
  });
