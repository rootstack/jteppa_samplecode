'use strict';

/**
 * @ngdoc directive
 * @name caldeyroApp.directive:Footer
 * @description
 * # Footer
 */


 angular.module('caldeyroApp')
 .directive('caldeyroFooter', FooterDirective);

 function FooterDirective() {
  return {
    restrict: 'E',
    scope: {
    },
    templateUrl: 'views/partial/footer.html',
    controller:[
        '$scope',
        '$rootScope',
        'MenuService',
        '$state',
        '$q',
        function ($scope, $rootScope, MenuService, $state, $q) {
      /*@ngInject*/
      var footer = this;

      footer.lang = $rootScope.lang;

      var loadMenu = function() {
        return $q.all({
          menu: MenuService.query(true, 'menu-caldeyro---footer')
        }).then(
          function(results) {
            if (_.isObject(results.menu) && !_.isUndefined(results.menu.menu)) {
              footer.menu = results.menu.menu;
            }
          }
        );
      };
      footer.setLang = function(lang) {
        $rootScope.setLang(lang);
      };

      var activate = function () {
        loadMenu();
      };
      activate();

      $scope.$on('langChanged', function(evt, key) {
        footer.lang = key;
        loadMenu();
      });
    }],
    controllerAs: 'footer',
    bindToController: true
  };
}


