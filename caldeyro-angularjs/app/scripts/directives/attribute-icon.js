'use strict';
 /*jshint unused:false*/
/**
 * @ngdoc directive
 * @name caldeyroApp.directive:attributeIcon
 * @description
 * # attributeIcon
 */
angular.module('caldeyroApp')
  .directive('attributeIcon', function () {
    return {
      template: '<span class="property-attribute {{icon}}"></div>',
      restrict: 'E',
      scope: {
        type: '='
      },
      link: function postLink(scope, element, attrs) {
        scope.icon = 'bedroom-icon';
        switch (scope.type) {
          case 'room':
            scope.icon = 'bedroom-icon';
            break;
          case 'toilet':
            scope.icon = 'bathroom-icon';
            break;
          case 'level':
            scope.icon = 'map-area-icon';
            break;
          case 'area': 
            scope.icon = 'area-icon';
            break;
          case 'garage': 
            scope.icon = 'garage-icon';
            break;
          case 'ic': 
            scope.icon = 'ic-icon';
            break;
          case 'floors': 
            scope.icon = 'stairs-icon';
            break;
          case 'service-room': 
            scope.icon = 'service-room';
            break;
          default:
        }
      }
    };
  });

angular.module('caldeyroApp').directive('errSrc', function errorSrc() {
  var directive = {
    restrict: 'A',
    link: function(scope, element, attrs) {

      element.bind('error', function() {
        if (attrs.src !== attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });

      attrs.$observe('ngSrc', function(value) {
        if (!value && attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
    }
  };
  return directive;
});

