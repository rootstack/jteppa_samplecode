'use strict';

/**
 * @ngdoc directive
 * @name caldeyroApp.directive:Responsive Menu
 * @description
 * # Responsive Menu
 */


angular.module('caldeyroApp')
  .directive('responsiveMenu', ResponsiveMenuDirective);

function ResponsiveMenuDirective() {
  var directive = {
    restrict: 'E',
    templateUrl: 'views/partial/caldeyro-menu-responsive.html',
    controller: [
      '$scope',
      '$rootScope',
      'MenuService',
      '$q',
      'screenSize',
      'snapRemote',
      function($scope, $rootScope, MenuService, $q, screenSize, snapRemote) {
        /*@ngInject*/
        var mobileMenu = this;

        mobileMenu.lang = $rootScope.lang;
        $scope.$on('langChanged', function(evt, key) {
          mobileMenu.lang = key;
          activate();
        });

        var processExternalLinks = function(data) {
          var exp = new RegExp('^(?:[a-z]+:)?//', 'i');
          _.each(data, function(menu, i) {
            if (!_.isUndefined(menu.id)) {
              menu.isExternal = exp.test(menu.id);
            }
            if (_.isObject(menu.items)) {
              processExternalLinks(menu.items)
            }
          });
        };

        var activate = function() {
          $q.all({
            menu: MenuService.query(true, 'menu-caldeyro-v2---header-main-m')
          }).then(function(results) {
            if (results && results.menu && results.menu.menu) {
              var tmp = results.menu.menu;
              processExternalLinks(tmp)
              mobileMenu.menu = tmp;
            }
          });
        };

        mobileMenu.setLang = function(lang) {
          $rootScope.setLang(lang);
        };

        activate();
      }
    ],
    controllerAs: 'mobileMenu',
    bindToController: true
  };
  return directive;
}