'use strict';
/*jshint latedef: nofunc */
/**
 * @ngdoc directive
 * @name caldeyroApp.directive:facetItem
 * @description
 * # facetItem
 */
angular.module('caldeyroApp')
  .directive('facetItem', facetItem);

function facetItem() {
  var directive = {
    link: link,
    restrict: 'E',
    scope: {
      item: '=',
      type: '='
    },
    template: '<div ng-include="vm.getTemplateUrl()"></div>',
    controller: 'FacetItemController',
    controllerAs: 'vm',
    bindToController: true
  };
  return directive;

  function link(scope, element, attrs, ctrl) {
    ctrl.getTemplateUrl = function() {
      var template = '';
      switch (ctrl.type) {
        case 'checkbox':
          template = 'views/facet/item-checkbox.html';
          break;
        case 'link':
          template = 'views/facet/item-link.html';
          break;
        case 'checkbox-list':
          template = 'views/facet/item-checkbox-link.html';
          break;
        default:
          template = 'views/facet/item-link.html';
      }

      return template;
    };
  }
}