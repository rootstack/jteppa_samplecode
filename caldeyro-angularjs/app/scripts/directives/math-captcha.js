'use strict';

/**
 * @ngdoc directive
 * @name caldeyroApp.directive:mathCaptcha
 * @description
 * # mathCaptcha
 */

angular.module('caldeyroApp')
  .directive('mathCaptcha', mathCaptcha);

function mathCaptcha() {
  var directive = {
    link: link,
    restrict: 'E',
    scope: {
      validate: '='
    },
    controllerAs: 'vm',
    controller: ['$scope', function($scope) {
      /*@ngInject*/
      var vm = this;

      $scope.$watch(
        function() { return vm.result; },
        function(newValue, oldValue) {
          if (newValue !== oldValue) {
            vm.validate = (mathenticate.solve() === vm.result);
          }
        }
      );

      var mathenticate = {
        bounds: {
          lower: 5,
          upper: 75
        },
        first: 0,
        second: 0,
        generate: function() {
          this.first = Math.floor(Math.random() * this.bounds.lower) + 1;
          this.second = Math.floor(Math.random() * this.bounds.upper) + 1;
        },
        solve: function() {
          return this.first + this.second;
        }
      };

      function reset() {
        mathenticate.generate();
        vm.first = mathenticate.first;
        vm.second = mathenticate.second;
      }

      reset();

      vm.reset = reset;
    }],
    templateUrl: 'views/forms/math-captcha.html',
    bindToController: true
  };
  return directive;

  function link() {

  }
}