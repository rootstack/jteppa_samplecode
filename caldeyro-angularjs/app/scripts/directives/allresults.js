'use strict';

/**
 * @ngdoc directive
 * @name caldeyroApp.directive:AllResults
 * @description
 * # AllResults
 */
angular.module('caldeyroApp')
  .directive('allResults', function() {
    return {
      templateUrl: 'views/partial/all-results.html',
      restrict: 'E',
      controller: ['$rootScope', 'SearchService', '$state', '$q', '$http', '$translate',
        function($rootScope, SearchService, $state, $q, $http, $translate) {
          /*@ngInject*/
          var vm = this;
          vm.total = 0;
          vm.clearFacets = function() {
            SearchService.text = '';
            SearchService.location = null;
            SearchService.reset = false;
            $rootScope.searching = true;
            var url = 'es/busqueda';
            if ($translate.use() == 'en') {
              url = 'en/search';
            }
            window.location = url;
          };

          var activate = function() {
            window.prerenderReady = false;
            $q.all({
              counter: $http({
                method: 'GET',
                url: 'https://api.caldeyro.com/es/services/caldeyro_total_result/' + $rootScope.lang
              })
            }).then(function(results) {
              if (results.counter && results.counter.data && results.counter.data.total) {
                vm.total = results.counter.data.total;
              } else {
                vm.total = 0;
              }
              window.prerenderReady = true;
            }, function() {
              vm.total = 0;
            });
          };

          activate();
        }
      ],
      controllerAs: 'all',
      link: function postLink() {}
    };
  });