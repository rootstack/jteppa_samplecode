'use strict';

/**
 * @ngdoc directive
 * @name caldeyroApp.directive:searchField
 * @description
 * # searchField
 */
 angular.module('caldeyroApp')
 .directive('searchField', function () {
  return {
    templateUrl: 'views/partial/search-field.html',
    restrict: 'E',
    link: function postLink() {
    },
    controller: ['$rootScope', '$state', 'SearchService', 'screenSize', function($rootScope, $state, SearchService, screenSize) {
      /*@ngInject*/
     var search = this;

     search.locations = [
     {name: 'Uruguay', id: 61},
     {name: 'Paraguay', id: 100},
     {name: 'Francia', id: 101},
     {name: 'Argentina', id: 97},
     {name: 'Chile', id: 99},
     {name: 'Protugal', id: 66},
     {name: 'Usa', id: 104},
     {name: 'Panama', id: 102},
     {name: 'Brasil', id: 98},
     {name: 'Canada', id: 103}

     ];

     search.keyDown = function(event) {
      if(event.keyCode === 13) {
        search.textSearch();
      }
    };
    search.location = SearchService.location ? SearchService.location : search.locations[0];
    search.keyword = SearchService.text;
    
    search.textSearch = function() {
      SearchService.text = search.keyword;
      SearchService.location = search.location;
      $state.go('app.results', {
       lang: $rootScope.lang,
       keyword: search.keyword,
       location: search.location.id,
       force: true
     }, {
       reload: true,
       inherit: false,
       notify: true
     });
    };

    if(screenSize.is('xs')) {
      search.imageSize = 'xs';
    } else if(screenSize.is('sm')) {
      search.imageSize = 'sm';
    } else if(screenSize.is('md')) {
      search.imageSize = 'md';
    } else if(screenSize.is('lg')) {
      search.imageSize = 'lg';
    }

    screenSize.when('xs', function() {
      search.imageSize = 'xs';
    });
    screenSize.when('sm', function() {
      search.imageSize = 'sm';
    });
    screenSize.when('md', function() {
      search.imageSize = 'md';
    });
    screenSize.when('lg', function() {
      search.imageSize = 'lg';
    });

  }],
  controllerAs:'search'
};
});
