'use strict';
/*jshint latedef: nofunc */
/**
 * @ngdoc directive
 * @name caldeyroApp.directive:facetCategory
 * @description
 * # facetCategory
 */

 angular.module('caldeyroApp')
 .directive('facetCategory', facetCategory);

 function facetCategory() {
  var directive = {
    link: link,
    restrict: 'E',
    scope: {
      category : '='
    },
    template: '<div ng-include="vm.getTemplateUrl()"></div>',
    controller:'FacetCategoryController',
    controllerAs: 'vm',
    bindToController: true
  };
  return directive;

  function link(scope, element, attrs, ctrl) {

    ctrl.getTemplateUrl = function() {
      var template = '';
      if(ctrl.category && ctrl.category.key) {
        if(ctrl.category.key === 'field_propiedad_categoria' || ctrl.category.key === 'field_propiedad_ubicacion') {
          template = 'views/facet/category-tree.html';
        } else {
          switch(ctrl.category.widget) {
            case 'search_api_ranges_ui_select':
            template = 'views/facet/category-select.html';
            break;
            case 'facetapi_checkbox_links':
            template = 'views/facet/category-checkbox-link.html';
            break;
            case 'facetapi_links':
            template = 'views/facet/category-links.html';
            break;
          }
        }
      }
      return template;
    };
  }
}
