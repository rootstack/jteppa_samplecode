'use strict';

/**
 * @ngdoc directive
 * @name caldeyroApp.directive:Header
 * @description
 * # Header
 */


angular.module('caldeyroApp')
  .directive('caldeyroHeader', HeaderDirective);

function HeaderDirective() {
  var directive = {
    link: link,
    restrict: 'E',
    scope: {
      type: '='
    },
    template: '<div ng-include="header.getTemplateUrl()"></div>',
    controller: [
      '$scope',
      '$rootScope',
      'MenuService',
      '$q',
      'SearchService',
      '$state',
      '$translate',
      '$location',
      function($scope, $rootScope, MenuService, $q, SearchService, $state, $translate, $location) {
        /*@ngInject*/
        var header = this;
        header.opened = false;
        header.lang = $rootScope.lang;

        $scope.$on('langChanged', function(evt, key) {
          header.lang = key;
          activate(true);
        });

        var processExternalLinks = function(data) {
          var exp = new RegExp('^(?:[a-z]+:)?//', 'i');
          _.each(data, function(menu, i) {
            if (!_.isUndefined(menu.id)) {
              menu.isExternal = exp.test(menu.id);
            }
            if (_.isObject(menu.items)) {
              processExternalLinks(menu.items)
            }
          });
        };

        var activate = function(changeRoute) {
          $q.all({
            menu: MenuService.query(true, 'menu-caldeyro-v2---header-main-m')
          }).then(function(results) {
            header.menu = results.menu.menu;
            processExternalLinks(header.menu);
            if ($state.current.name == "app.page.static" && changeRoute) {
              MenuService.goToTranslatedUrl();
            }
          });
        };

        header.setLang = function(lang) {
          $rootScope.setLang(lang);
        };

        header.goToHome = function() {
          var route = $translate.use() == 'en' ? 'search' : 'busqueda';
          window.open($translate.use() + '/' + route, '_blank');
        };

        header.clearFacets = function() {
          SearchService.text = '';
          SearchService.location = null;
          SearchService.reset = true;
          $state.go('app.results', {
            lang: $rootScope.lang,
            force: true
          }, {
            reload: true,
            inherit: false,
            notify: true
          });
        };

        header.mouseEnter = function(item) {
          if (item && item.items && _.isArray(item.items) && item.items.length > 0) {
            header.opened = true;
          }
        }

        activate();
      }
    ],
    controllerAs: 'header',
    bindToController: true
  };
  return directive;

  function link(scope, element, attrs, ctrl) {
    ctrl.getTemplateUrl = function() {
      var template = '';
      switch (ctrl.type) {
        case 'small':
          template = 'views/partial/small-header.html';
          break;
        default:
          template = 'views/partial/big-header.html';
      }

      return template;
    };
  }
}