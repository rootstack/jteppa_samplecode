'use strict';

/**
 * @ngdoc directive
 * @name caldeyroApp.directive:caldeyroForm
 * @description
 * # caldeyroForm
 */

angular.module('caldeyroApp')
  .directive('caldeyroForm', caldeyroForm);

caldeyroForm.$inject = ['$state'];

function caldeyroForm($state) {
  var directive = {
    link: link,
    restrict: 'E',
    scope: {
      form: '=',
      property: '=?'
    },
    controllerAs: 'vm',
    controller: ['$scope', '$http', '$translate', function($scope, $http, $translate) {
      /*@ngInject*/
      var vm = this;
      vm.lang = $translate.use();
      vm.captchaResponse = {};
      vm.captcha = false;

      vm.honey = null;

      $scope.$on('langChanged', function(evt, key) {
        vm.lang = key;
      });

      var getFlatValue = function(val) {
        return ((_.isArray(val) ? _.head(val) : val) || null);
      };

      vm.sendForm = function() {
        if (vm.validate) {
          var data = {};
          angular.forEach(vm.form.field, function(field) {
            if (field.value) {
              data[field.form_key] = field.value;
            }
          });
          data['propertyref'] = vm.honey
          // Add property related data
          if (_.isObject(vm.property)) {
            if (_.isObject(vm.property.fields)) {
              if (!_.isUndefined(vm.property.fields.field_ref)) {
                data.refweb = getFlatValue(vm.property.fields.field_ref);
              }
            }
          } else {
            data.refweb = 'contacto';
          }

          $http({
            method: 'POST',
            url: 'https://api.caldeyro.com/' + vm.form.url,
            transformRequest: function(obj) {
              var str = [];
              for (var p in obj) {
                str.push('data[' + encodeURIComponent(p) + ']=' + encodeURIComponent(obj[p]));
              }
              var encoded = str.join('&');
              return encoded;
            },
            data: data,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          }).then(function() {
              $state.go('app.page.thanks');
            },
            function() { // optional
              $state.go('app.page.error');
            });
        } else {

        }
      };
    }],
    templateUrl: 'views/forms/caldeyro-form.html',
    bindToController: true
  };
  return directive;

  function link() {

  }
}