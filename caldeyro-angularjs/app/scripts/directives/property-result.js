'use strict';
/*jshint latedef: nofunc */
/*jshint unused:false*/
/**
 * @ngdoc directive
 * @name caldeyroApp.directive:propertyResult
 * @description
 * # propertyResult
 */
angular.module('caldeyroApp')
  .directive('propertyResult', propertyResult);

function propertyResult() {
  var directive = {
    link: link,
    restrict: 'E',
    scope: {
      property: '=',
      size: '=',
      viewMode: '=',
      complex: '=',
      home: '=?'
    },
    controllerAs: 'vm',
    controller: ['$scope', '$location', '$rootScope', '$state', function($scope, $location, $rootScope, $state) {
      /*@ngInject*/
      var vm = this;

      // Format property fields.

      vm.attrs = 0;

      vm.lang = $rootScope.lang;
      vm.propertyText = vm.lang == 'es' ? 'propiedad' : 'property';

      $scope.$on('langChanged', function(evt, key) {
        vm.lang = key;
        vm.propertyText = vm.lang == 'es' ? 'propiedad' : 'property';
      });
      vm.details = false;

      var toggleDetails = function() {
        vm.details = !vm.details;
      };

      var gotoPropertyPage = function() {
        $state.go('app.page.property', {
          lang: vm.lang,
          title: vm.property.parsedTitle
        });
      };

      vm.onSliderStart = function(slider) {

      };

      vm.gotoPropertyPage = gotoPropertyPage;

      vm.toggleDetails = toggleDetails;
    }],
    templateUrl: 'views/search/property-result.html',
    bindToController: true
  };
  return directive;

  function link(scope, element, attrs, ctrl) {

  }
}