'use strict';

/**
 * @ngdoc overview
 * @name caldeyroApp
 * @description
 * # caldeyroApp
 *
 * Main module of the application.
 */
angular
  .module('caldeyroApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'snap',
    'matchMedia',
    'pascalprecht.translate',
    'ncy-angular-breadcrumb',
    'ui.bootstrap',
    'ui.router',
    'anim-in-out',
    'angular-flexslider',
    'ngMap',
    'vcRecaptcha',
    'duScroll',
    '720kb.socialshare',
    'updateMeta',
    'seo'
  ])
  .config([
    '$stateProvider',
    'snapRemoteProvider',
    '$translateProvider',
    '$urlRouterProvider',
    '$httpProvider',
    '$locationProvider',
    '$urlMatcherFactoryProvider',
    '$resourceProvider',
    function(
      $stateProvider,
      snapRemoteProvider,
      $translateProvider,
      $urlRouterProvider,
      $httpProvider,
      $locationProvider,
      $urlMatcherFactoryProvider,
      $resourceProvider) {
      /*@ngInject*/
      $locationProvider.html5Mode(true).hashPrefix('!'); //the hashPrefix is for SEO

      $resourceProvider.defaults.cancellable = true;

      $httpProvider.defaults.headers.common = {};

      $httpProvider.defaults.headers.post = {};
      $httpProvider.defaults.headers.put = {};
      $httpProvider.defaults.headers.patch = {};


      var $http,
        interceptor = ['$q', '$injector', function($q, $injector) {
          function success(response) {
            window.prerenderReady = false;
            $http = $http || $injector.get('$http');
            var $timeout = $injector.get('$timeout');
            var $rootScope = $injector.get('$rootScope');
            if ($http.pendingRequests.length < 1) {
              $timeout(function() {
                if ($http.pendingRequests.length < 1) {
                  $rootScope.htmlReady();
                  $rootScope.status = 'ready';
                  // window.prerenderReady = true;
                }
              }, 1.5); //an 0.7 seconds safety interval, if there are no requests for 0.7 seconds, it means that the app is through rendering
            }
            return response;
          }

          function error(response) {
            $http = $http || $injector.get('$http');
            window.prerenderReady = false;
            return $q.reject(response);
          }

          return {
            'request': function(config) {
              var $rootScope = $injector.get('$rootScope');
              $rootScope.status = 'notready';
              window.prerenderReady = false;
              return config;
            },
            'response': success,
            'responseError': error
          }
        }];

      $httpProvider.interceptors.push(interceptor);

      function valToString(val)   { return val != null ? val.toString().replace(/\//g, "%2F") : val; }

      function valFromString(val) { return val != null ? val.toString().replace(/%2F/g, "/")  : val; }

      $urlMatcherFactoryProvider.type('nonURIEncoded', {
        encode: valFromString,
        decode: valToString,
        is: function() { return true; }
      });

      $stateProvider
        .state('app', {
          abstract: true,
          url: '/{lang:(?:es|en)}',
          template: '<ui-view/>'
        })
        .state('app.page', {
          templateUrl: 'views/layout/small-header-page.html'
        })
        .state('app.page.not-found', {
          url: '/404',
          templateUrl: 'views/partial/404.html',
        })
        .state('app.page.error', {
          url: '/500',
          templateUrl: 'views/partial/500.html',
        })
        .state('app.page.thanks', {
          url: '/{url:(?:thanks|gracias)}',
          templateUrl: 'views/partial/thanks.html',
          params: {
            url: 'gracias'
          }
        })
        .state('app.page.property', {
          url: '/{url:(?:propiedad|property)}/{title:.*}',
          templateUrl: 'views/partial/property.html',
          controller: 'PropertyCtrl',
          controllerAs: 'vm',
          params: {
            url: ['$stateParams', function($stateParams) {
              /*@ngInject*/
              return $stateParams.lang === 'es' ? 'propiedad' : 'property';
            }]
          },
          resolve: {
            property: ['$http', '$stateParams', '$q', '$rootScope', function($http, $stateParams, $q, $rootScope) {
              /*@ngInject*/

              function firstUpperCase(value) {
                var calculatedValue = '';

                if (value) {
                  console.log(value);
                  calculatedValue = value.join(' ');
                  if (calculatedValue.length > 1) {
                    calculatedValue = calculatedValue[0].toUpperCase() + calculatedValue.substring(1);
                    calculatedValue = [calculatedValue];
                    console.log('NOW ', calculatedValue);
                  } else {
                    calculatedValue = [];
                  }
                } else {
                  calculatedValue = null;
                }
                return calculatedValue;
              }
              var deferred = $q.defer();
              window.prerenderReady = false;
              $http({
                method: 'GET',
                url: 'https://api.caldeyro.com/services/caldeyro_info_node/contenido?lang=' + $stateParams.lang + '&path_alias=' + $stateParams.url + '/' + $stateParams.title
              }).then(function(data) {
                if (
                  data.data.count &&
                  data.data.count > 0 &&
                  data.data.data &&
                  data.data.data.length > 0) {
                  //metatags
                  $rootScope.ogkeywords = data.data.metatag['Keywords'] || '';
                  $rootScope.ogdescription = data.data.metatag['Description'] || '';
                  $rootScope.title = data.data.metatag['Page title'] || '';
                  $rootScope.title = $rootScope.title.toUpperCase();
                  $rootScope.ogtitle = $rootScope.title;
                  $rootScope.ogtype = "webpage";

                  var property = data.data.data[0];
                  property.location = (property
                      .fields['field_propiedad_ubicacion:name'] || [])
                    .reverse()
                    .join(', ');

                  property.fields.more_features = firstUpperCase(property.fields.more_features);
                  property.fields.observations = firstUpperCase(property.fields.observations);
                  property.fields['field_descripcion_construcciones:value'] = firstUpperCase(property.fields['field_descripcion_construcciones:value']);
                  property.fields['field_descripcion_tierra:value'] = firstUpperCase(property.fields['field_descripcion_tierra:value']);

                  deferred.resolve(property);
                } else {
                  deferred.reject('HOME');
                }
              }, function() {
                deferred.reject('HOME');
              });
              return deferred.promise;
            }],
            form: ['Form', '$rootScope', '$q', function(Form, $rootScope, $q) {
              /*@ngInject*/
              var deferred = $q.defer();
              Form.get(false, $rootScope.lang, 'property_form').then(function(data) {
                deferred.resolve(data);
              }, function() {
                deferred.reject('NOT_FOUND');
              });
              return deferred.promise;
            }]
          }
        })
        .state('app.page.complex', {
          url: '/{url:(?:complex|complejo)}/{title:nonURIEncoded}',
          templateUrl: 'views/partial/complex.html',
          controller: 'ComplexCtrl',
          controllerAs: 'vm',
          params: {
            url: ['$stateParams', function($stateParams) {
              /*@ngInject*/
              return $stateParams.lang === 'es' ? 'complejo' : 'complex';
            }]
          },
          resolve: {
            data: ['$http', '$stateParams', '$q', function($http, $stateParams, $q) {
              /*@ngInject*/
              window.prerenderReady = false;
              var deferred = $q.defer();
              var key = ($stateParams.lang == "es" ? 'complejo/' : 'complex/') + $stateParams.title;
              $http({
                method: 'GET',
                url: 'https://api.caldeyro.com/services/caldeyro_info_node/contenido?lang=' + $stateParams.lang + '&path_alias=' + key + '&filter[type]=complejo'
              }).then(function(response) {
                if (
                  response &&
                  response.data &&
                  response.data.count &&
                  response.data.count > 0 &&
                  response.data.data &&
                  response.data.data.valueOf("0") &&
                  response.data.data.valueOf("0")[0]) {

                  var data = {
                    property: response.data.data.valueOf("0")[0],
                    property_related: _.map(response
                      .data.data.valueOf("0")
                      .property_related,
                      function(item) {
                        return item[0];
                      })
                  };

                  _.map(data.property_related, function(item) {

                    item.location = (item.fields['field_propiedad_ubicacion:name'] || [])
                      .reverse()
                      .join(', ');

                    var coneat = parseInt(item.fields.field_indice_prod_promedio, 10);
                    if (_.isNumber(coneat) && coneat > 0) {
                      item.fields.field_indice_prod_promedio = coneat;
                    } else {
                      item.fields.field_indice_prod_promedio = null;
                    }

                    var title = item.fields.field_node_link;
                    item.parsedTitle = title;

                  });


                  data.property.location = (data.property
                      .fields['field_propiedad_ubicacion:name'] || [])
                    .reverse()
                    .join(', ');

                  data.property.fields['field_descripcion_construcciones:value'] = (data.property
                      .fields['field_descripcion_construcciones:value'] || [])
                    .join("<br>").split("&lt;p&gt;")
                    .join("").split("&lt;/p&gt;").join("");

                  deferred.resolve(data);
                } else {
                  deferred.reject('HOME');
                }
              }, function() {
                deferred.reject('HOME');
              });
              return deferred.promise;
            }],
            form: ['Form', '$rootScope', '$q', function(Form, $rootScope, $q) {
              /*@ngInject*/
              var deferred = $q.defer();
              Form.get(false, $rootScope.lang, 'property_form').then(function(data) {
                deferred.resolve(data);
              }, function() {
                deferred.reject('NOT_FOUND');
              });
              return deferred.promise;
            }]
          }
        })
        .state('app.page.static', {
          url: '/{url:(?:informacion|information)}/{urlTitle:nonURIEncoded}',
          templateUrl: 'views/partial/static.html',
          controller: 'StaticCtrl',
          controllerAs: 'vm',
          params: {
            url: ['$stateParams', function($stateParams) {
              /*@ngInject*/
              return $stateParams.lang === 'es' ? 'informacion' : 'information';
            }],
            title: null,
            parent: null,
            urlTitle: null
          },
          resolve: {
            title: ['$stateParams', function($stateParams) {
              return $stateParams.title;
            }],
            page: ['$stateParams', 'staticPageParser', '$http', '$state', '$q', '$rootScope', 'MenuService', function($stateParams, staticPageParser, $http, $state, $q, $rootScope, MenuService) {
              /*@ngInject*/
              window.prerenderReady = false;
              var deferred = $q.defer();
              var key = ($stateParams.lang == "es" ? 'informacion/' : 'information/') + $stateParams.urlTitle;
              $http({ method: 'GET', url: 'https://api.caldeyro.com/services/caldeyro_info_page/contenido?lang=' + $stateParams.lang + '&path_alias=' + key }).then(function(data) {
                if (data && data.data) {
                  $rootScope.ogkeywords = data.data.metatag['Keywords'] || '';
                  $rootScope.ogdescription = data.data.metatag['Description'] || '';
                  $rootScope.title = data.data.metatag['Page title'] || '';
                  $rootScope.title = $rootScope.title.toUpperCase();
                  $rootScope.ogtitle = $rootScope.title;
                  $rootScope.ogtype = "webpage";

                  var translatedUrl = data.data.translate;
                  if ($rootScope.lang == 'es') {
                    translatedUrl = 'en/' + translatedUrl.en;
                  } else {
                    translatedUrl = 'es/' + translatedUrl.es;
                  }

                  MenuService.setTranslatedUrl(translatedUrl);

                  deferred.resolve(staticPageParser.parse(data.data));
                } else {
                  deferred.reject('NOT_FOUND');
                }
              });
              return deferred.promise;
            }],
            form: ['Form', '$rootScope', '$q', '$translate', function(Form, $rootScope, $q, $translate) {
              /*@ngInject*/
              var deferred = $q.defer();
              Form.get(false, $translate.use(), 'contact_form').then(function(data) {
                deferred.resolve(data);
              }, function() {
                deferred.reject('NOT_FOUND');
              });
              return deferred.promise;
            }],
            menu: ['MenuService', '$q', function(MenuService, $q) {
              /*@ngInject*/
              var deferred = $q.defer();
              window.prerenderReady = false;
              MenuService.query(false, 'menu-caldeyro-v2---right-menu').then(function(data) {
                deferred.resolve(data);
              }, function() {
                deferred.reject('NOT_FOUND');
              });
              return deferred.promise;
            }],
            parent: ['$stateParams', function($stateParams) {
              /*@ngInject*/
              return $stateParams.parent;
            }]
          }
        })
        .state('app.results', {
          url: '/{url:(?:busqueda|search)}/{filters:.*}',
          templateUrl: 'views/partial/main.html',
          controller: 'MainCtrl',
          controllerAs: 'main',
          cache: false,
          params: {
            url: ['$stateParams', function($stateParams) {
              /*@ngInject*/
              return $stateParams.lang === 'es' ? 'busqueda' : 'search';
            }],
            keyword: null,
            force: true
          }
        });

      $urlRouterProvider.otherwise(function($injector) {
        $injector.invoke(['$rootScope', '$state', function($rootScope, $state) {
          'ngInject';
          if ($rootScope.isFirstLoad) {
            $rootScope.isFirstLoad = false;
            $state.go('app.results', { lang: $rootScope.lang });
          } else {
            $state.go('app.page.not-found');
          }
          return true;
        }]);
      });


      $translateProvider.translations('en', {
        'NATURALS': 'Naturals',
        'ACCESS_STATUS': 'Access status ',
        'WATERY': 'Watery ',
        'DESCRIPTION': 'Description',
        'SUITE': 'In suite',
        'TOILET': 'Toilette',
        'SERVICE': 'Service',
        'OBSERVATIONS': 'Observations',
        'available_units': 'Available Units',
        'unidad-o-lote': 'Unit',
        'in': 'in',
        'CONEAT_INDEX': 'Coneat Index:',
        'ALL': 'All',
        'CLEAR_FILTERS': 'See all results',
        'CLEAR_FILTERS_MOBILE': 'Clear filters',
        'RESULTS_PER_PAGE': 'Results per page',
        'SEE_MORE': 'See More',
        'SEE_LESS': 'See Less',
        'SALE_PRICE': 'Sale',
        'SALE_PRICE_PER_UNIT': 'Sale per {{unit}}',
        'RENT_PRICE': 'Rent',
        'SEND': 'Send',
        'RESET': 'Reset',
        'LOCATION': 'Location',
        'MATH_QUESTION': 'Math Question',
        'SOLVE_THIS': 'Solve this simple problem math problem and enter te result. E.g for 1 + 3, enter 4',
        'FIELD_REQUIRED': 'This field is required',
        'FIELD_INVALID_ERROR': 'This field has an invalid value',
        'FIELD_INVALID_EMAIL_ERROR': 'This field has an invalid email',
        'CALDEYRO_ASSOC': 'Caldeyro Victorica Real Estate is a member of:',
        'CALDEYRO_FOOTER_TITLE_1': 'Caldeyro Victorica',
        'CALDEYRO_FOOTER_TITLE_2': 'Real Estate',
        'CALDEYRO_FOOTER_ADDRESS': 'Acapulco 1605 | CP 11500 | Carrasco Montevideo, Uruguay ',
        'CALDEYRO_FOOTER_CONTACT': 'Stay in contact with us:',
        'field_aptitud_o_potencial': 'Potential',
        'field_banos': 'Baths',
        'field_caract_grales_exteriores': 'General Caracteristics',
        'field_dormitorios_totales': 'Rooms',
        'field_estado': 'Status',
        'field_propiedad_ubicacion': 'Location',
        'field_propiedad_categoria': 'Category',
        'field_vistas': 'Views',
        'field_precio_venta:amount': 'Sales Price',
        'field_precio_alquiler:amount': 'Rent Price',
        'field_mconstruidos': 'Area',
        'field_indice_prod_promedio': 'Productivity',
        'search_api_aggregation_1': 'Land Area',
        'search_api_aggregation_2': 'Hectareas',
        'SEE_PROPERTY': 'See Property',
        'BEDROOM': 'Bedroom',
        'CONSTRUCTED': 'Constructed',
        'BATHROOM': 'Bathroom',
        'LAND': 'Land',
        'GARAGES': 'Garages',
        'STATUS': 'Status',
        'HOUSE': 'House',
        'MORE_CHARACTERISTICS': 'More Characteristics',
        'GENERAL_CHARACTERISTICS': 'Characteristics',
        'GENERAL_CONDITIONS': 'General Conditions',
        'BACK_TO_SEARCH_RESULTS': 'Back to search results',
        'NAME': 'Name',
        'EMAIL': 'E-mail',
        'COMMENTS': 'Comments',
        'COUNTRY': 'Country',
        'ABOUT_US': 'About Us',
        'THANKS_MESSAGE': 'Thanks for contacting us, we will reply to you as soon as possible.',
        'RESULTS_TEXT': 'Showing from {{from}} to {{to}} of {{total}} results...',
        'NEWER': 'Newest first',
        'RESULTS_TEXT_MOBILE': '{{total}} properties',
        'SALES_PRICE_DESC': 'Sale price descending',
        'SALES_PRICE_ASC': 'Sale price ascending',
        'RENTAL_PRICE_DESC': 'Rental price descending',
        'RENTAL_PRICE_ASC': 'Rental price ascending',
        'LAND_AREA_DESC': 'Land area descending',
        'LAND_AREA_ASC': 'Land area ascending',
        'BUILDED_AREA_DESC': 'Living area approx. descending',
        'BUILDED_AREA_ASC': 'Living area approx. ascending',
        'ROOMS_DESC': 'Bedrooms descending',
        'ROOMS_ASC': 'Bedrooms ascending',
        'information': 'information',
        'SEARCH_BY_KEYWORD': 'Enter keyword',
        'NO_RESULTS_PROPERTY': 'Your search yields no results. Broaden search ...',
        'SEARCH': 'Search',
        'HOME': 'Home',
        'REVICE_OUR_NEWSLETTER': 'Receive Our Newsletter',
        'FOLLOW_US': 'Follow us on:',
        'FILTER': 'Filter',
        'GO_BACK': 'Go back',
        'ORDER_BY': 'Order by',
        'FILTERS': 'Filters',
        'SUBSCRIBE': 'Subscribe',
        'DELETE_ALL_FILTERS': 'Reset all filters',
        'FLOORS': 'Floors',
        'CONTACTO': 'Contact',
        'SUBSCRIBING': 'Sending...',
        'AND': 'and',
        "request_more_info": "REQUEST MORE INFORMATION",
        "search_for_properties_caldeyro_victorica": "Search for properties | Caldeyro Victorica"
      });

      $translateProvider.translations('es', {
        'CONTACTO': 'Contacto',
        'NATURALS': 'Naturales',
        'ACCESS_STATUS': 'Estado de los accesos',
        'WATERY': 'Aguada ',
        'DESCRIPTION': 'Descripción',
        'SUITE': 'En suite',
        'TOILET': 'Toilette',
        'SERVICE': 'Servicio',
        'OBSERVATIONS': 'Observaciones',
        'available_units': 'Unidades Disponibles',
        'unidad-o-lote': 'Unidad',
        'in': 'en',
        'CONEAT_INDEX': 'Índice Coneat',
        'ALL': 'Todos',
        'CLEAR_FILTERS': 'Ver todos los resultados',
        'CLEAR_FILTERS_MOBILE': 'Limpiar filtros',
        'RESULTS_PER_PAGE': 'Resultados por página',
        'SEE_MORE': 'Ver Más',
        'SEE_LESS': 'Ver Menos',
        'SALE_PRICE': 'Venta',
        'SALE_PRICE_PER_UNIT': 'Venta por {{unit}}',
        'RENT_PRICE': 'Alquiler',
        'SEND': 'Enviar',
        'RESET': 'Resetear',
        'LOCATION': 'Ubicación',
        'MATH_QUESTION': 'Pregunta Matematica',
        'SOLVE_THIS': 'Solucione este problema simple de matematicas e ingrese el resultado. E.g para 1 + 3, ingrese 4',
        'FIELD_REQUIRED': 'Este campo es requerido',
        'FIELD_INVALID_ERROR': 'Este campo tiene un valor invalido',
        'FIELD_INVALID_EMAIL_ERROR': 'Este campo tiene un email invalido',
        'CALDEYRO_ASSOC': 'Caldeyro Victorica Bienes Raíces es socio de:',
        'CALDEYRO_FOOTER_TITLE_1': 'Caldeyro Victorica',
        'CALDEYRO_FOOTER_TITLE_2': 'Bienes Raíces',
        'CALDEYRO_FOOTER_ADDRESS': 'Acapulco 1605 esq. Mantua | CP 11500 | Carrasco Montevideo, Uruguay ',
        'CALDEYRO_FOOTER_CONTACT': 'Estamos en contacto en:',
        'field_aptitud_o_potencial': 'Potencial',
        'field_banos': 'Baths',
        'field_caract_grales_exteriores': 'Características Generales',
        'field_dormitorios_totales': 'Dormitorios',
        'field_estado': 'Estados',
        'field_propiedad_ubicacion': 'Ubicación',
        'field_propiedad_categoria': 'Categoría',
        'field_vistas': 'Vistas',
        'field_precio_venta:amount': 'Precio de Venta',
        'field_precio_alquiler:amount': 'Precio Alquiler',
        'field_mconstruidos': 'Metros Construidos',
        'field_indice_prod_promedio': 'Productividad Promedio',
        'search_api_aggregation_1': 'Superficie Terreno',
        'search_api_aggregation_2': 'Hectareas',
        'SEE_PROPERTY': 'Ver Propiedad',
        'BEDROOM': 'Dormitorios',
        'CONSTRUCTED': 'Construido',
        'BATHROOM': 'Baños',
        'LAND': 'Terreno',
        'GARAGES': 'Garages',
        'STATUS': 'Estado',
        'HOUSE': 'Casa',
        'MORE_CHARACTERISTICS': 'Más Características',
        'GENERAL_CHARACTERISTICS': 'Características',
        'GENERAL_CONDITIONS': 'Condiciones Generales',
        'BACK_TO_SEARCH_RESULTS': 'Volver a los resultados de busqueda',
        'NAME': 'Nombre',
        'EMAIL': 'E-mail',
        'COMMENTS': 'Comentarios',
        'COUNTRY': 'Pais',
        'MOST_VIEWED': 'Propiedades más vistas',
        'ABOUT_US': 'Quienes Somos',
        'THANKS_MESSAGE': 'Gracias por contactarse con nosotros. Pronto le estaremos respondiendo.',
        'RESULTS_TEXT': 'Mostrando de {{from}} a {{to}} de {{total}} resultados...',
        'RESULTS_TEXT_MOBILE': '{{total}} propiedades',
        'NEWER': 'El más nuevo primero',
        'SALES_PRICE_DESC': 'Precio de venta descendente',
        'SALES_PRICE_ASC': 'Precio de venta ascendente',
        'RENTAL_PRICE_DESC': 'Precio de alquiler descendente',
        'RENTAL_PRICE_ASC': 'Precio de alquiler  ascendente',
        'LAND_AREA_DESC': 'Superficie del terreno descendente',
        'LAND_AREA_ASC': 'Superficie del terreno ascendente',
        'BUILDED_AREA_DESC': 'Superficie construida aprox. descendente',
        'BUILDED_AREA_ASC': 'Superficie construida aprox. ascendente',
        'ROOMS_DESC': 'Cuartos descendente',
        'ROOMS_ASC': 'Cuartos ascendente',
        'information': 'información',
        'SEARCH_BY_KEYWORD': 'Ingrese palabra clave',
        'NO_RESULTS_PROPERTY': 'Su búsqueda no arrojó resultados. Amplíe los criterios de búsqueda…',
        'SEARCH': 'Búsqueda',
        'HOME': 'Inicio',
        'REVICE_OUR_NEWSLETTER': 'Recibe Nuestro Newsletter',
        'SUBSCRIBE': 'Suscribirse',
        'FOLLOW_US': 'Síguenos en:',
        'FILTER': 'Filtrar',
        'GO_BACK': 'Regresar',
        'ORDER_BY': 'Ordenar por',
        'FILTERS': 'Filtros',
        'DELETE_ALL_FILTERS': 'Eliminar todos los filtros',
        'FLOORS': 'Plantas',
        'SUBSCRIBING': 'Enviando...',
        'AND': 'y',
        "request_more_info": "CONTACTO",
        "search_for_properties_caldeyro_victorica": "Búsqueda de propiedades y complejos | Caldeyro Victorica"
      });

      $translateProvider.preferredLanguage('es');
      $translateProvider.useSanitizeValueStrategy('escape');

      snapRemoteProvider.globalOptions = {
        disable: 'right',
        touchToDrag: false
      };
    }
  ])
  .run(['$rootScope',
    'snapRemote',
    '$translate',
    '$state',
    '$stateParams',
    'MetaTags',
    '$location',
    'screenSize',
    '$anchorScroll',
    '$http',
    '$filter',
    function($rootScope, snapRemote, $translate, $state, $stateParams, MetaTags, $location, screenSize, $anchorScroll, $http, $filter) {
      /*@ngInject*/
      $rootScope.lang = 'es';
      $rootScope.isFirstLoad = true;
      $rootScope.metaTags = [];
      $rootScope.title = "";
      $rootScope.$on('$stateChangeSuccess', function rootStateChangeSuccess() {
        $rootScope.searching = false;
        if ($stateParams.lang !== undefined) {
          if ($stateParams.lang !== $rootScope.lang) {
            $rootScope.setLang($stateParams.lang);
          }
        }
        $rootScope.showSmall = true;
        $rootScope.goToTop();
        snapRemote.close();
      });
      
      $rootScope.$on('$stateChangeStart', function rootScopeStateChangeError(event, toState, toParams) {
        $rootScope.searching = true;
        if (toState.name === 'app.results') {
          if (toParams.lang === 'es' && toParams.url === 'search') {
            event.preventDefault();
            $state.go('app.results', { lang: 'es', url: 'busqueda' });
          } else if (toParams.lang === 'en' && toParams.url === 'busqueda') {
            event.preventDefault();
            $state.go('app.results', { lang: 'en', url: 'search' });
          }
        } else if (toState.name === 'app.page.thanks') {
          if (toParams.lang === 'es' && toParams.url === 'thanks') {
            event.preventDefault();
            $state.go('app.page.thanks', { lang: 'es', url: 'gracias' });
          } else if (toParams.lang === 'en' && toParams.url === 'gracias') {
            event.preventDefault();
            $state.go('app.page.thanks', { lang: 'en', url: 'thanks' });
          }
        }
      });

      $rootScope.$on('$stateChangeError', function rootScopeStateChangeError(event, stateFrom) {

        event.preventDefault();
        $rootScope.searching = false;

        if (stateFrom.name == "app.page.property") {
          $state.go('app.results', { lang: $rootScope.lang });
        } else {
          $state.go('app.page.not-found', { lang: $rootScope.lang });
        }
        $location.replace();
      });

      $rootScope.$on('meta-tags-updated', function() {
        $rootScope.metaTags = MetaTags.getAll();
      });

      $rootScope.setLang = function(lang) {
        $rootScope.lang = lang;
        $translate.use(lang);
        $state.go($state.current, { lang: $rootScope.lang }, { reload: true }, {force: true});
      };

      $rootScope.$watch(function() {
        return $translate.use();
      }, function(newValue, oldValue) {
        if (newValue !== oldValue) {
          $rootScope.$broadcast('langChanged', newValue);
        }
      });

      // Show alternative fixed header
      $rootScope.desktop = screenSize.on('md, lg', function(match) {
        $rootScope.desktop = match;
      });

      snapRemote.getSnapper().then(function(snapper) {
        snapper.on('open', function() {
          $('.main-content').addClass('opened');
        });

        snapper.on('close', function() {
          $('.main-content').removeClass('opened');
        });
      });

      var id = "/en";
      if ($location.url().slice(-id.length) == id) {
        $rootScope.lang = 'en';
        $translate.use($rootScope.lang);
        $state.go('app.results', { lang: 'en' });
      } else {
        $state.go('app.results', { lang: 'es' });
      }

      $rootScope.news = {
        email: '',
        name: ''
      };
      $rootScope.subscribing = false;
      $rootScope.subscribe = function() {
        $rootScope.subscribing = true;
        var data = {
          'name': $rootScope.news.name,
          'email': $rootScope.news.email
        };
        var formId = ($rootScope.lang == 'es' ? 49 : 50);
        $http({
          method: 'POST',
          url: 'https://api.caldeyro.com/services/caldeyro_form_api/' + formId,
          transformRequest: function(obj) {
            var str = [];
            for (var p in obj) {
              str.push('data[' + encodeURIComponent(p) + ']=' + encodeURIComponent(obj[p]));
            }
            var encoded = str.join('&');
            return encoded;
          },
          data: data,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function() {
          $rootScope.subscribing = false;
          $state.go('app.page.thanks');
        }, function() { // optional
          $rootScope.subscribing = false;
          $state.go('app.page.error');
        });
      };
      $rootScope.goToTop = function() {
        $anchorScroll('top');
      };

      $rootScope.searchResults = true;

      $rootScope.translate = function (KEY, obj) {
        return $filter('translate')(KEY, (obj || {}));
      };
    }
  ]).filter('capitalize', function() {
    return function(token) {
      if (token) {
        return token.charAt(0).toUpperCase() + token.slice(1).toLowerCase();
      } else {
        return token;
      }

    }
  }).filter('money', ['$rootScope', '$translate', function($rootScope, $translate) {
    return function(value) {
      var decimal_positions = 0;
      var decimal_delimiter = '.';
      var thousand_delimiter = ',';
      if ($translate.use() == 'es') {
        decimal_delimiter = ',';
        thousand_delimiter = '.';
      }

      var n = parseFloat(value, 10);
      if (_.isNumber(n)) {
        return n.toFixed(decimal_positions).replace(/./g, function(c, i, a) {
          return i && c !== decimal_delimiter && ((a.length - i) % 3 === 0) ? thousand_delimiter + c : c;
        });

      }
      return value;
    };
  }]).filter('capitalize-first', function() {
    return function(token) {
      if (token && _.isString(token) && token.length > 0) {
        return token.charAt(0).toUpperCase() + token.slice(1);
      }

      return "";
    }
  })
  .filter('h1', function (){
    return function(str){
      return str.replace(" | Caldeyro Victorica", "");
    }
  });