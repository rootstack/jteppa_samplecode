<?php

namespace Drupal\pt_moderation\Service;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\node\NodeInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Class NodeSummaryService.
 */
class ModerationNotificationSender implements ModerationNotificationSenderInterface {

  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $queryFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  protected $currentUser;

  protected $mailManager;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  protected $langCode;

  /**
   * Constructs a new NodeSummaryService object.
   */
  public function __construct(QueryFactory $query_factory, EntityTypeManager $entity_type_manager, LoggerChannelFactoryInterface $logger_factory, MailManagerInterface $mail_manager, AccountProxyInterface $current_user) {
    $this->queryFactory = $query_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('pt_moderation');

    $this->mailManager = $mail_manager;

    $user = $current_user->id();
    $this->currentUser = User::load($user);
    $this->langCode = LanguageInterface::LANGCODE_SITE_DEFAULT;
  }

  /**
   * {@inheritdoc}
   */
  public function sendNotification(NodeInterface $entity, $moderation_state) {
    switch ($moderation_state) {
      case self::NEEDS_REVIEW:
        $this->needsReviewNotification($entity);
        break;

      case self::PUBLISHED:
        $this->publishedNotification($entity);
        break;

      case self::NEEDS_WORK:
        $this->needsWorkNotification($entity);
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function publishedNotification($entity) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $revisions = $node_storage->revisionIds($entity);

    if (empty($revisions) || count($revisions) < 2) {
      return;
    }

    end($revisions);
    $current_revision = prev($revisions);
    $revision = $node_storage->loadRevision($current_revision);

    // If node was published on previous revision, do not send.
    if ($revision->get('moderation_state')->value == self::PUBLISHED) {
      return;
    }

    /** @var \Drupal\user\UserInterface $user */
    $user = $revision->getRevisionUser();
    $params['node_title'] = $entity->getTitle();

    $params['mail_title'] = 'Su contenido de tipo ' . $entity->type->entity->label() . ' ha sido publicado.';

    $body[] = 'El nodo <strong>' . $params['node_title'] . '</strong> de tipo <strong>' . $entity->type->entity->label() . '</strong> ha sido publicado.';
    $body[] = 'Puede visitar el mismo en el siguiente enlace:';
    $body[] = \Drupal::request()->getSchemeAndHttpHost() . \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $entity->id());
    $params['body'] = $body;

    $this->sendMail($user, $params);
  }

  /**
   * {@inheritdoc}
   */
  protected function needsReviewNotification($entity) {
    if ($this->currentUser->hasRole('editor_aig')) {
      return;
    }

    $ids = $this->queryFactory->get('user')
      ->condition('status', 1)
      ->condition('roles', 'editor_aig')
      ->execute();
    $users = User::loadMultiple($ids);

    $params['mail_title'] = $entity->type->entity->label() . ' enviado para revisión.';
    $params['node_title'] = $entity->getTitle();


    $body[] = 'El nodo <strong>' . $params['node_title'] . '</strong> de tipo <strong>' . $entity->type->entity->label() . '</strong> ha sido enviado para su revisión.';
    $body[] = 'Para ver todos los nodos pendientes por revisión ingrese al siguiente enlace:';
    $body[] = \Drupal::request()->getSchemeAndHttpHost() . '/admin/content';
    $params['body'] = $body;

    /** @var \Drupal\user\Entity\User $user */
    foreach ($users as $user) {
      $this->sendMail($user, $params);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function needsWorkNotification($entity) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $revisions = $node_storage->revisionIds($entity);

    if (empty($revisions) || count($revisions) < 2) {
      return;
    }

    end($revisions);
    $current_revision = prev($revisions);
    $revision = $node_storage->loadRevision($current_revision);

    // If node was needs work on previous revision, do not send.
    if ($revision->get('moderation_state')->value == self::NEEDS_WORK) {
      return;
    }

    /** @var \Drupal\user\UserInterface $user */
    $user = $revision->getRevisionUser();

    $params['mail_title'] = 'Su contenido de tipo ' . $entity->type->entity->label() . ' necesita subsanación.';
    $params['node_title'] = $entity->getTitle();

    $body[] = '<strong>' . $entity->type->entity->label() . ' - ' . $params['node_title'] . '</strong> necesita subsanación.';
    if (!empty($entity->getRevisionLogMessage())) {
      $body[] = '<h4><strong>Motivos de rechazo:</strong></h4>';
      $body[] = '<div style="color: red">' . nl2br($entity->getRevisionLogMessage()) . '</div>';
    }
    $body[] = 'Para subsanar el contenido diríjase al siguiente enlace ' . \Drupal::request()->getSchemeAndHttpHost() . '/user';
    $body[] = 'Recuerde que debe ingresar en el módulo administrativo. El contenido a subsanar estará en la sección "Pendientes por corregir"';

    $params['body'] = $body;
    $this->sendMail($user, $params);

  }

  /**
   * {@inheritdoc}
   */
  protected function sendMail($user, $params) {
    $result = $this->mailManager->mail(self::MODULE_NAME, self::MAILKEY, $user->getEmail(), $this->langCode, $params, NULL, TRUE);
    if ($result['result'] !== TRUE) {
      $this->logger->error(t('Failed to send email notification when updating content.'));
    }
  }

}
