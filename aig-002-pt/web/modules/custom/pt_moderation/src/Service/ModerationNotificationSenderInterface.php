<?php

namespace Drupal\pt_moderation\Service;

use Drupal\node\NodeInterface;

/**
 * Interface NodeSummaryInterface.
 */
interface ModerationNotificationSenderInterface {

  const MODULE_NAME = 'pt_moderation';
  const MAILKEY = 'content_ready_review';
  const NEEDS_REVIEW = 'needs_review';
  const PUBLISHED = 'published';
  const NEEDS_WORK = 'needs_work';

  /**
   * Sends the node notification.
*
* @param \Drupal\node\NodeInterface $entity
*   The entity that will be used for the notification.
   * @param string $moderation_state
*   The current moderation state.
   *
   * @return bool
  *   Whether or not the notification was sent.
   */
  public function sendNotification(NodeInterface $entity, $moderation_state);

}
