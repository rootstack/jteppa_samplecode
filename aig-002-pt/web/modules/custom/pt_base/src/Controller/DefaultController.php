<?php

namespace Drupal\pt_base\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  /**
   * Home.
   *
   * @return array
   *   Return render array.
   */
  public function home() {
    $build = [];

    return $build;
  }

}
