<?php

namespace Drupal\pt_base\Service;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\node\Entity\Node;

/**
 * Class NodeSummaryService.
 */
class NodeSummaryService implements NodeSummaryInterface {

  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $queryFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a new NodeSummaryService object.
   */
  public function __construct(QueryFactory $query_factory, EntityTypeManager $entity_type_manager) {
    $this->queryFactory = $query_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeAmount($contentType, $published, array $extraConditions = []) {
    $published = $published ? Node::PUBLISHED : Node::NOT_PUBLISHED;

    switch ($contentType) {
      case NodeSummaryInterface::PROCEDURE:
        $amount = $this->fetchAmount('tramite', $published, $extraConditions);
        break;

      case NodeSummaryInterface::INSTITUTION:
        $amount = $this->fetchAmount('institution', $published, $extraConditions);
        break;

      case NodeSummaryInterface::ONLINE_PROCEDURE:
        $amount = $this->fetchAmount(
          'tramite',
          $published,
          array_merge([['field_link', 0, '>']], $extraConditions)
        );
        break;

      case NodeSummaryInterface::ONLINE_SERVICE:
        $amount = $this->fetchAmount('online_service', $published, $extraConditions);
        break;

      case NodeSummaryInterface::MUNICIPALITY:
        $amount = $this->fetchAmount(
          'institution',
          $published,
          array_merge(
            [['field_institution_type', $this->getMunicipalitiesTids(), 'IN']],
            $extraConditions
          )
        );
        break;

      case NodeSummaryInterface::DEPENDENCY:
        $amount = $this->fetchAmount('dependency', $published, $extraConditions);
        break;

      case NodeSummaryInterface::ARTICLE:
        $amount = $this->fetchAmount('article', $published, $extraConditions);
        break;

      default:
        $amount = 0;
        break;
    }

    return $amount;
  }

  /**
   * Fetch amount of nodes.
   *
   * @param string $contentType
   *   Content type.
   * @param int $published
   *   Published status.
   * @param array $extraConditions
   *   Extra Conditions.
   *
   * @return int
   *   Amount of nodes.
   */
  private function fetchAmount($contentType, $published, array $extraConditions) {
    $query = $this->queryFactory->get('node')
      ->condition('type', $contentType)
      ->condition('status', $published);

    foreach ($extraConditions as $condition) {
      $query = $query->condition($condition[0], $condition[1], $condition[2]);
    }

    return $query->count()->execute();
  }

  /**
   * Get municipalities term ids.
   *
   * @return array
   *   Municipalities Term Ids
   */
  private function getMunicipalitiesTids() {
    $municipalitiesTids = [0];
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree('institution_type');

    foreach ($terms as $term) {
      if ($term->name === 'Municipios') {
        $municipalitiesTids[] = $term->tid;
      }
    }

    return $municipalitiesTids;
  }

}
