<?php

namespace Drupal\pt_base\Service;

/**
 * Interface NodeSummaryInterface.
 */
interface NodeSummaryInterface {

  public const PROCEDURE = 0;
  public const INSTITUTION = 1;
  public const ONLINE_PROCEDURE = 2;
  public const ONLINE_SERVICE = 3;
  public const MUNICIPALITY = 4;
  public const DEPENDENCY = 5;
  public const ARTICLE = 6;

  /**
   * Get node amount.
   *
   * @param string $contentType
   *   Content type.
   * @param int $published
   *   Published.
   * @param array $extraConditions
   *   Extra conditions.
   *
   * @return int
   *   Amount.
   */
  public function getNodeAmount($contentType, $published, array $extraConditions = []);

}
