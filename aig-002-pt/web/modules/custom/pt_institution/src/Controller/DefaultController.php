<?php

namespace Drupal\pt_institution\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormBuilder;
use Drupal\facets\Utility\FacetsUrlGenerator;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $queryFactory;

  /**
   * Drupal\facets\Utility\FacetsUrlGenerator definition.
   *
   * @var \Drupal\facets\Utility\FacetsUrlGenerator
   */
  protected $facetsUrlGenerator;

  /**
   * Drupal\Core\Form\FormBuilder definition.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * Constructs a new DefaultController object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    QueryFactory $query_factory,
    FacetsUrlGenerator $facets_url_generator,
    FormBuilder $form_builder
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->queryFactory = $query_factory;
    $this->facetsUrlGenerator = $facets_url_generator;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity.query'),
      $container->get('facets.utility.url_generator'),
      $container->get('form_builder')
    );
  }

  /**
   * Build institution list.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return array
   *   Return render array.
   */
  public function build(Request $request) {
    $showCategory = NULL !== $request->query->get('category');

    if ($showCategory) {
      $content['categories']['view'] = $this->listCategories($request);
    }
    else {
      $content['institutions']['view'] = [
        '#type' => 'view',
        '#name' => 'institutions_search_api',
        '#display_id' => 'list',
      ];
    }

    return $content;
  }

  /**
   * List categories.
   *
   * @return array
   *   Return render array.
   */
  private function listCategories(Request $request) {
    $taxonomyStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $vid = 'institution_type';
    $filters = $this->getFilters($request->query);

    $institutionTypes = $taxonomyStorage->loadTree($vid);
    $filteredInstitutionTypes = $this->filterInstitutionTypes($institutionTypes, $filters);
    $builtInstitutionTypes = $this->buildInstitutionTypes($filteredInstitutionTypes, $filters);
    $finalInstitutionTypes = $this->removeEmptyInstitutionTypes($builtInstitutionTypes);

    return [
      '#theme' => 'pt_institution',
      '#types' => $finalInstitutionTypes,
      '#attached' => [
        'library' => [
          'pt_institution/pt-category-block',
        ],
      ],
    ];
  }

  /**
   * Get Filters from Query.
   *
   * @param Symfony\Component\HttpFoundation\ParameterBag $query
   *   Query.
   *
   * @return array
   *   Return filters in query.
   */
  private function getFilters(ParameterBag $query) {
    $f = $query->get('f') ?? [];
    $filters = [];
    foreach ($f as $parameter) {
      $data = explode(':', $parameter);
      $filters[$data[0]] = $data[1];
    }
    return $filters;
  }

  /**
   * Filter Institution Types.
   *
   * @param array $institutionTypes
   *   Institution Types.
   * @param array $filters
   *   Filters.
   *
   * @return array
   *   Return filtered institution types.
   */
  private function filterInstitutionTypes(array $institutionTypes, array $filters) {
    return array_filter($institutionTypes, function ($institutionType) use ($filters) {
      if (!isset($filters['institution_type'])) {
        return TRUE;
      }
      return $institutionType->tid === $filters['institution_type'];
    });
  }

  /**
   * Build Institution types as array.
   *
   * @param array $institutionTypes
   *   Institution Types.
   * @param array $filters
   *   Filters.
   *
   * @return array
   *   Return built institution types.
   */
  private function buildInstitutionTypes(array $institutionTypes, array $filters) {
    return array_map(function ($institutionType) use ($filters) {
      $query = $this->queryFactory->get('node')
        ->condition('type', 'institution')
        ->condition('status', Node::PUBLISHED)
        ->condition('field_institution_type', $institutionType->tid);
      if (isset($filters['provincia'])) {
        $query->condition('field_contact_information.entity.field_province.value', $filters['provincia']);
      }
      $institutionAmount = $query->count()->execute();

      $url = $this->facetsUrlGenerator->getUrl(['institution_type' => [$institutionType->tid]], FALSE);
      $options = $url->getOptions();
      unset($options['query']['category']);
      $url = $url->setOptions($options);

      return [
        'institutionAmount' => $institutionAmount,
        'title' => $institutionType->name,
        'description' => $institutionType->description__value,
        'url' => $url->toString(),
      ];
    }, $institutionTypes);
  }

  /**
   * Remove institution types with zero institutions.
   *
   * @param array $institutionTypes
   *   Built Institution Types.
   *
   * @return array
   *   Return institution types that have at least one institution.
   */
  private function removeEmptyInstitutionTypes(array $institutionTypes) {
    return array_filter($institutionTypes, function ($institutionType) {
      return $institutionType['institutionAmount'] > 0;
    });
  }

}
