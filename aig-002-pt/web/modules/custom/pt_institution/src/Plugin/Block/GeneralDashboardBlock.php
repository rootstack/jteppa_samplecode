<?php

namespace Drupal\pt_institution\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\Entity\Node;
use Drupal\pt_base\Service\NodeSummaryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'GeneralDashboardBlock' block.
 *
 * @Block(
 *  id = "general_dashboard_block_institution",
 *  admin_label = @Translation("General Dashboard block Institution"),
 * )
 */
class GeneralDashboardBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\pt_base\Service\NodeSummaryInterface definition.
   *
   * @var \Drupal\pt_base\Service\NodeSummaryInterface
   */
  protected $nodeSummary;

  /**
   * Constructs a new CategoryBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   AccountProxy definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   EntityType Manager.
   * @param \Drupal\pt_base\Service\NodeSummaryInterface $node_summary
   *   Node summary definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    NodeSummaryInterface $node_summary
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->nodeSummary = $node_summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('node_summary')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $userId = $this->currentUser->getAccount()->id();
    $user = $this->entityTypeManager->getStorage('user')->load($userId);

    $institutionId = $user->get('field_institution')->getValue()[0]['target_id'];
    $institution = $this->entityTypeManager->getStorage('node')->load($institutionId);

    $nodesData = [
      NodeSummaryInterface::DEPENDENCY => ['name' => $this->t('Dependencies')],
      NodeSummaryInterface::PROCEDURE => ['name' => $this->t('Procedures')],
    ];

    foreach ($nodesData as $contentType => $nodeData) {
      $nodesData[$contentType]['published'] = $this->nodeSummary->getNodeAmount(
        $contentType, Node::PUBLISHED,
        [['field_institution', $institutionId, '=']]
      );
      $nodesData[$contentType]['not_published'] = $this->nodeSummary->getNodeAmount(
        $contentType, Node::NOT_PUBLISHED,
        [['field_institution', $institutionId, '=']]
      );
    }

    return [
      '#theme' => 'pt_institution_dashboard',
      '#institution' => $institution,
      '#nodesData' => $nodesData,
    ];
  }

}
