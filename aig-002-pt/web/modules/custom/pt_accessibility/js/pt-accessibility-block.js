/* eslint-disable */
(function ($) {

  Drupal.behaviors.pt_accessibility_global = {
    attach: function (context, settings) {
      $('html', context).once('accessibility-feature').each(function () {
        var $this = $(this);
        if (localStorage.getItem('font-size') !== null) {
          $this.addClass('a11y-font-' + localStorage.getItem('font-size'));
        }
        else {
          $this.addClass('a11y-font-0');
        }

        if (localStorage.getItem('high-contrast') !== null) {
          $this.addClass('a11y-contrast');
        }
      });
    }
  };

  Drupal.behaviors.pt_accessibility_block = {
    attach: function (context, settings) {
      var MIN_FONT = 0;
      var MAX_FONT = 2;
      var $html = $('html');

      $('.accessibility-container', context).on('click', '.font-reduce', function () {
        if (isNaN(parseInt(localStorage.getItem('font-size')))) {
          localStorage.setItem('font-size', 0);
        }

        if (parseInt(localStorage.getItem('font-size')) > MIN_FONT) {
          $html.removeClass(function (index, className) {
            return (className.match(/(^|\s)a11y-font-\S+/g) || []).join(' ');
          });
          localStorage.setItem('font-size', parseInt(localStorage.getItem('font-size')) - 1);
          $('html').addClass('a11y-font-' + localStorage.getItem('font-size'));
        }
      });
      $('.accessibility-container', context).on('click', '.font-increase', function () {
        if (isNaN(parseInt(localStorage.getItem('font-size')))) {
          localStorage.setItem('font-size', 0);
        }
        
        if (parseInt(localStorage.getItem('font-size')) < MAX_FONT) {
          $html.removeClass(function (index, className) {
            return (className.match(/(^|\s)a11y-font-\S+/g) || []).join(' ');
          });
          localStorage.setItem('font-size', parseInt(localStorage.getItem('font-size')) + 1);
          $html.addClass('a11y-font-' + localStorage.getItem('font-size'));
        }
      });

      $('.accessibility-container', context).on('click', '.high-contrast', function () {
        if (localStorage.getItem('high-contrast') === null) {
          localStorage.setItem('high-contrast', true);
          $html.addClass('a11y-contrast');
        }
        else {
          localStorage.removeItem('high-contrast');
          $html.removeClass('a11y-contrast');
        }
      });
    }
  };
})(jQuery);
