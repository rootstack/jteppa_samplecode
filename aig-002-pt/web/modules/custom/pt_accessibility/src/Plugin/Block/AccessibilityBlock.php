<?php

namespace Drupal\pt_accessibility\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'AccessibilityBlock' block.
 *
 * @Block(
 *  id = "accessibility_block",
 *  admin_label = @Translation("Accessibility block"),
 * )
 */
class AccessibilityBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#theme' => 'pt_accessibility',
      '#attached' => [
        'library' => [
          'pt_accessibility/pt-accessibility-block',
        ],
      ],
    ];

    return $build;
  }

}
