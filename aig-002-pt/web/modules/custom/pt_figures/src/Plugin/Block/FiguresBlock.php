<?php

namespace Drupal\pt_figures\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node\Entity\Node;
use Drupal\pt_base\Service\NodeSummaryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Figures' Block.
 *
 * @Block(
 *   id = "figures_block",
 *   admin_label = @Translation("Node Figures"),
 *   category = @Translation("Figures"),
 * )
 */
class FiguresBlock extends BlockBase implements ContainerFactoryPluginInterface, BlockPluginInterface {
  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $queryFactory;

  /**
   * Drupal\pt_base\Service\NodeSummaryInterface definition.
   *
   * @var \Drupal\pt_base\Service\NodeSummaryInterface
   */
  protected $nodeSummary;

  /**
   * Constructs a new RelatedContentBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\Query\QueryFactory $query_factory
   *   QueryFactory Service.
   * @param \Drupal\pt_base\Service\NodeSummaryInterface $node_summary
   *   NodeSummaryInterface Service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    QueryFactory $query_factory,
    NodeSummaryInterface $node_summary
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->queryFactory = $query_factory;
    $this->nodeSummary = $node_summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.query'),
      $container->get('node_summary')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $procedures = [
      'description' => !empty($config['procedures_description']) ? $config['procedures_description'] : '',
    ];
    $institutions = [
      'description' => !empty($config['institutions_description']) ? $config['institutions_description'] : '',
    ];
    $onlineProcedures = [
      'description' => !empty($config['online_procedures_description']) ? $config['online_procedures_description'] : '',
    ];
    $onlineServices = [
      'description' => !empty($config['online_services_description']) ? $config['online_services_description'] : '',
    ];
    $municipalities = [
      'description' => !empty($config['municipalities_description']) ? $config['municipalities_description'] : '',
    ];

    $procedures['count'] = $this->nodeSummary->getNodeAmount(NodeSummaryInterface::PROCEDURE, Node::PUBLISHED);
    $institutions['count'] = $this->nodeSummary->getNodeAmount(NodeSummaryInterface::INSTITUTION, Node::PUBLISHED);
    $onlineProcedures['count'] = $this->nodeSummary->getNodeAmount(NodeSummaryInterface::ONLINE_PROCEDURE, Node::PUBLISHED);
    $onlineServices['count'] = $this->nodeSummary->getNodeAmount(NodeSummaryInterface::ONLINE_SERVICE, Node::PUBLISHED);
    $municipalities['count'] = $this->nodeSummary->getNodeAmount(NodeSummaryInterface::MUNICIPALITY, Node::PUBLISHED);

    $build = [
      '#theme' => 'pt_figures',
      '#procedures' => $procedures,
      '#institutions' => $institutions,
      '#onlineProcedures' => $onlineProcedures,
      '#onlineServices' => $onlineServices,
      '#municipalities' => $municipalities,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['procedures_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Procedures'),
      '#description' => $this->t("Procedure's description for count block"),
      '#default_value' => $config['procedures_description'] ?? '',
    ];

    $form['institutions_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Institutions'),
      '#description' => $this->t("Institution's description for count block"),
      '#default_value' => $config['institutions_description'] ?? '',
    ];

    $form['online_procedures_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Online Procedures'),
      '#description' => $this->t("Procedure's description for count block"),
      '#default_value' => $config['online_procedures_description'] ?? '',
    ];

    $form['online_services_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Online Services'),
      '#description' => $this->t("Procedure's description for count block"),
      '#default_value' => $config['online_services_description'] ?? '',
    ];

    $form['municipalities_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Municipalities'),
      '#description' => $this->t("Municipalities description for count block"),
      '#default_value' => $config['municipalities_description'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();

    $this->configuration['procedures_description'] = $values['procedures_description'];
    $this->configuration['institutions_description'] = $values['institutions_description'];
    $this->configuration['online_procedures_description'] = $values['online_procedures_description'];
    $this->configuration['online_services_description'] = $values['online_services_description'];
    $this->configuration['municipalities_description'] = $values['municipalities_description'];
  }

}
