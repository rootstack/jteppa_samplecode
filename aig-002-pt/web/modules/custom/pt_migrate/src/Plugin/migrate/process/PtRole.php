<?php

namespace Drupal\pt_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Transforms the Role names.
 *
 * @MigrateProcessPlugin(
 *   id = "pt_role"
 * )
 */
class PtRole extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $roles_mapping = [
      1 => 'anonymous',
      2 => 'authenticated',
      3 => 'administrator',
      4 => 'editor_aig',
      5 => 'editor_institution',
    ];

    return $roles_mapping[$value];
  }

}
