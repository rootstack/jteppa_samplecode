<?php

namespace Drupal\pt_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;

/**
 * Drupal 7 extension for the node migration for procedures.
 *
 * @MigrateSource(
 *   id = "pt_node_procedure",
 *   source_module = "node"
 * )
 */
class PtNodeProcedure extends PtNode {

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    // Add a virtual field for the migration for the NY Teacher featured field.
    $fields += [
      'field_tramite_requirements' => $this->t('Procedure requirements'),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $nid = $row->getSourceProperty('nid');

    // Check flag value on source and set NY Teacher featured field accordingly.
    $requirementCollectionIds = $this->select('field_data_field_tramite_requirements', 'fr')
      ->fields('fr', ['field_tramite_requirements_value'])
      ->condition('fr.bundle', 'tramite')
      ->condition('fr.deleted', 0)
      ->condition('fr.entity_id', $nid)
      ->execute()
      ->fetchCol();

    $collections = [];
    foreach ($requirementCollectionIds as $requirementCollectionId) {
      $requirementIds = $this->select('field_data_field_requisito', 'r')
        ->fields('r', ['field_requisito_nid'])
        ->condition('r.bundle', 'field_tramite_requirements')
        ->condition('r.deleted', 0)
        ->condition('r.entity_id', $requirementCollectionId)
        ->execute()
        ->fetchCol();
      $requirementTypeId = $this->select('field_data_field_requisito_interes', 'ri')
        ->fields('ri', ['field_requisito_interes_tid'])
        ->condition('ri.bundle', 'field_tramite_requirements')
        ->condition('ri.deleted', 0)
        ->condition('ri.entity_id', $requirementCollectionId)
        ->execute()
        ->fetchCol();
      $requirementType = $this->select('taxonomy_term_data', 't')
          ->fields('t', ['name'])
          ->condition('t.tid', $requirementTypeId[0])
          ->execute()
          ->fetchCol();
      $collections[] = [
        'requirement_ids' => $requirementIds,
        'requirement_type' => $requirementType[0] ?? '',
      ];
    }
    $row->setDestinationProperty('field_tramite_requirements', $collections);
    return parent::prepareRow($row);
  }

}
