<?php

namespace Drupal\pt_migrate\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ProcedureEventSubscriber.
 *
 * Event subscribers for procedure imports.
 *
 * @package Drupal\pt_migrate
 */
class ProcedureEventSubscriber implements EventSubscriberInterface {

  const MIGRATION_ID = 'pt_d7_node_procedure';

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity query.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $entityQuery;

  /**
   * ProcedureEventSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   EntityType Manager.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   Entity Query service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, QueryFactory $entity_query) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityQuery = $entity_query;
  }

  /**
   * Get subscribed events.
   *
   * @inheritdoc
   */
  public static function getSubscribedEvents() {
    $events[MigrateEvents::POST_ROW_SAVE][] = ['onMigratePostRowSave'];
    return $events;
  }

  /**
   * Post import row actions, create and add paragraphs for procedure row.
   *
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   *   The import event object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onMigratePostRowSave(MigratePostRowSaveEvent $event) {
    if ($event->getMigration()->getBaseId() !== self::MIGRATION_ID) {
      return;
    }

    $row = $event->getRow();
    $source = $row->getSource();

    $provinceMapping = [
      '1' => 'Bocas del Toro',
      '2' => 'Coclé',
      '3' => 'Colón',
      '4' => 'Chiriquí',
      '5' => 'Darién',
      '6' => 'Herrera',
      '7' => 'Los Santos',
      '8' => 'Panamá',
      '9' => 'Veraguas',
      '10' => 'Comarca Ngobe Bugle',
      '11' => 'Comarca Guna Yala',
      '12' => 'Comarca Embera-Wounnan',
      'panama-oeste' => 'Panamá Oeste',
    ];

    $contactInformation = $this->entityTypeManager->getStorage('paragraph')
      ->create([
        'type' => 'contact_information',
        'field_address' => !empty($source['field_address'])
        ? [
          'value' => substr(html_entity_decode(strip_tags($source['field_address'][0]['value'])), 0, 500),
        ]
        : NULL,
        'field_address_map' => !empty($source['field_location'])
        ? ['lng' => $source['field_location'][0]['longitude'], 'lat' => $source['field_location'][0]['latitude']]
        : [],
        'field_province' => !empty($source['field_province']) ? $provinceMapping[$source['field_province'][0]['tid']] : NULL,
      ]);
    $contactInformation->save();

    $requirements = [];
    foreach ($row->getDestinationProperty('field_tramite_requirements') as $requirementData) {
      $requirement = $this->entityTypeManager->getStorage('paragraph')
        ->create([
          'type' => 'requirement_group',
          'field_requirement' => array_map(function ($requirementId) {
            return ['target_id' => $requirementId];
          }, $requirementData['requirement_ids']),
          'field_requirement_type' => $requirementData['requirement_type'],
        ]);
          $requirement->save();
          $requirements[] = $requirement;
    }

    $node = $this->entityTypeManager
      ->getStorage('node')
      ->load((int) $row->getDestination()['nid']);
    $node->set('field_contact_information', [
      'target_id' => $contactInformation->id(),
      'target_revision_id' => $contactInformation->getRevisionId(),
    ]);
    $node->set('field_requirements', array_map(function ($requirement) {
      return [
        'target_id' => $requirement->id(),
        'target_revision_id' => $requirement->getRevisionId(),
      ];
    }, $requirements));
    $node->save();
  }

}
