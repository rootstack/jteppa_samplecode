<?php

namespace Drupal\pt_migrate\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\migrate\Row;
use Drupal\node\Entity\Node;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class InstitutionEventSubscriber.
 *
 * Event subscribers for institution imports.
 *
 * @package Drupal\pt_migrate
 */
class InstitutionEventSubscriber implements EventSubscriberInterface {

  const MIGRATION_ID = 'pt_d7_node_institution';

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity query.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $entityQuery;

  /**
   * InstitutionEventSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   EntityType Manager.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   Entity Query service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, QueryFactory $entity_query) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityQuery = $entity_query;
  }

  /**
   * Get subscribed events.
   *
   * @inheritdoc
   */
  public static function getSubscribedEvents() {
    $events[MigrateEvents::POST_ROW_SAVE][] = ['onMigratePostRowSave'];
    $events[MigrateEvents::POST_IMPORT][] = ['onMigratePostImport'];
    return $events;
  }

  /**
   * Post import row actions.
   *
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   *   The import event object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onMigratePostRowSave(MigratePostRowSaveEvent $event) {
    if ($event->getMigration()->getBaseId() !== self::MIGRATION_ID) {
      return;
    }

    $row = $event->getRow();

    $this->assignInstitutionToUsers($row);
    $this->migrateCompleteInstitution($row);
  }

  /**
   * Updates the order of the contract pages node once they get created.
   *
   * @param \Drupal\migrate\Event\MigrateImportEvent $event
   *   The import event object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function onMigratePostImport(MigrateImportEvent $event) {
    if ($event->getMigration()->getBaseId() !== self::MIGRATION_ID) {
      return;
    }

    $this->setEntityToMigration('institution', 'field_institution_parent', 'field_parent_institution_legacy');
    $this->setEntityToMigration('tramite', 'field_institution', 'field_belong_institution_legacy');
    $this->setEntityToMigration('department', 'field_institution', 'field_institution_legacy');
    $this->setEntityToMigration('dependency', 'field_institution', 'field_inst_legacy');
    $this->setEntityToMigration('form', 'field_institution', 'field_institution_legacy_1');
  }

  /**
   * Assign institution to users.
   *
   * @param \Drupal\migrate\Row $row
   *   Row.
   */
  private function assignInstitutionToUsers(Row $row) {
    $institutionId = $row->getDestinationProperty('nid');

    $userIds = $this->entityQuery->get('user')
      ->condition('field_institution_legacy_id', (string) $institutionId)
      ->execute();
    $users = $this->entityTypeManager->getStorage('user')->loadMultiple($userIds);

    foreach ($users as $user) {
      $user->set('field_institution', [
        'target_id' => $institutionId,
        'target_type' => 'node',
      ]);
      $user->save();
    }
  }

  /**
   * Migrate rest of institution fields.
   *
   * @param \Drupal\migrate\Row $row
   *   Row.
   */
  private function migrateCompleteInstitution(Row $row) {
    $provinceMapping = [
      '1' => 'Bocas del Toro',
      '2' => 'Coclé',
      '3' => 'Colón',
      '4' => 'Chiriquí',
      '5' => 'Darién',
      '6' => 'Herrera',
      '7' => 'Los Santos',
      '8' => 'Panamá',
      '9' => 'Veraguas',
      '10' => 'Comarca Ngobe Bugle',
      '11' => 'Comarca Guna Yala',
      '12' => 'Comarca Embera-Wounnan',
      'panama-oeste' => 'Panamá Oeste',
    ];

    $phoneNumbers = [];
    foreach ($row->getSourceProperty('field_phone') as $fieldPhone) {
      $phoneParagraph = $this->entityTypeManager->getStorage('paragraph')->create([
        'type' => 'phone_numbers',
        'field_phone_number_description' => '',
        'field_phone_number' => $fieldPhone['value'],
      ]);
      $phoneParagraph->save();
      $phoneNumbers[] = [
        'target_id' => $phoneParagraph->id(),
        'target_revision_id' => $phoneParagraph->getRevisionId(),
      ];
    }

    $paragraph = $this->entityTypeManager->getStorage('paragraph')->create([
      'type' => 'contact_information',
      'field_address' => [
        'value' => substr(html_entity_decode(strip_tags($row->getSourceProperty('field_address')[0]['value'] ?? '')), 0, 500),
      ],
      'field_website' => $this->transformFieldLink(!empty($row->getSourceProperty('field_entidad_link')[0]) ? $row->getSourceProperty('field_entidad_link')[0] : NULL),
      'field_email' => array_map(function ($email) {
        return ['value' => $email['email']];
      }, $row->getSourceProperty('field_email')),
      'field_province' => !empty($row->getSourceProperty('field_province')) ? $provinceMapping[$row->getSourceProperty('field_province')[0]['tid']] : NULL,
      'field_phone_numbers' => $phoneNumbers,
    ]);
      $paragraph->save();

      $node = $this->entityTypeManager->getStorage('node')->load((int) $row->getDestinationProperty('nid'));
      $node->set('field_contact_information', [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ]);
      $node->set('field_social_networks', [
        'platform_values' => $this->getPlatformsFromLegacy($node),
      ]);
      $node->save();
  }

  /**
   * Get platform values from legacy links.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Node.
   *
   * @return array
   *   Platform values array.
   */
  private function getPlatformsFromLegacy(Node $node) {
    $platforms = [
      'twitter' => ['value' => ''],
      'facebook' => ['value' => ''],
    ];
    foreach (array_keys($platforms) as $platform) {
      $matches = [
        '/^https:\/\/www\.' . $platform . '\.com\//',
        '/^https:\/\/' . $platform . '\.com\//',
        '/^http:\/\/www\.' . $platform . '\.com\//',
        '/^http:\/\/' . $platform . '\.com\//',
      ];

      $url = !empty($node->get('field_' . $platform . '_legacy')->getValue())
        ? $node->get('field_' . $platform . '_legacy')->getValue()[0]['uri']
        : '';
      foreach ($matches as $match) {
        if (preg_match($match, $url)) {
          $platforms[$platform]['value'] = preg_replace($match, '', $url);
          break;
        }
      }
    }

    return $platforms;
  }

  /**
   * Add entity to migration.
   *
   * @param string $contentType
   *   Content type.
   * @param string $field
   *   Destination Field.
   * @param string $tempField
   *   Temporary field where nid is temporary saved.
   */
  private function setEntityToMigration($contentType, $field, $tempField) {
    $nids = $this->entityQuery->get('node')
      ->condition('type', $contentType)
      ->execute();
    $entities = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);

    foreach ($entities as $entity) {
      $entity->set($field, [
        'target_id' => !empty($entity->get($tempField)->getValue())
        ? (int) $entity->get($tempField)->getValue()[0]['value']
        : 0,
        'target_type' => 'node',
      ]);
      $entity->save();
    }
  }

  /**
   * Transform field link.
   *
   * @param array|null $value
   *   Value.
   *
   * @return array
   *   Route.
   */
  private function transformFieldLink($value) {
    if (NULL === $value) {
      return [];
    }

    $attributes = unserialize($value['attributes']);
    // Drupal 6/7 link attributes might be double serialized.
    if (!is_array($attributes)) {
      $attributes = unserialize($attributes);
    }

    if (!$attributes) {
      $attributes = [];
    }

    // Massage the values into the correct form for the link.
    $route['uri'] = $this->canonicalizeUri($value['url']);
    $route['options']['attributes'] = $attributes;
    $route['title'] = $value['title'];
    return $route;
  }

  /**
   * Canonicalize Uri.
   *
   * @param string $uri
   *   Uri.
   */
  private function canonicalizeUri($uri) {
    // If we already have a scheme, we're fine.
    if (empty($uri) || parse_url($uri, PHP_URL_SCHEME)) {
      return $uri;
    }

    // Remove the <front> component of the URL.
    if (strpos($uri, '<front>') === 0) {
      $uri = substr($uri, strlen('<front>'));
    }
    else {
      // List of unicode-encoded characters that were allowed in URLs,
      // according to link module in Drupal 7. Every character between &#x00BF;
      // and &#x00FF; (except × &#x00D7; and ÷ &#x00F7;) with the addition of
      // &#x0152;, &#x0153; and &#x0178;.
      // @see http://cgit.drupalcode.org/link/tree/link.module?h=7.x-1.5-beta2#n1382
      $link_ichars = '¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿŒœŸ';

      // Pattern specific to internal links.
      $internal_pattern = "/^(?:[a-z0-9" . $link_ichars . "_\-+\[\] ]+)";

      $directories = "(?:\/[a-z0-9" . $link_ichars . "_\-\.~+%=&,$'#!():;*@\[\]]*)*";
      // Yes, four backslashes == a single backslash.
      $query = "(?:\/?\?([?a-z0-9" . $link_ichars . "+_|\-\.~\/\\\\%=&,$'():;*@\[\]{} ]*))";
      $anchor = "(?:#[a-z0-9" . $link_ichars . "_\-\.~+%=&,$'():;*@\[\]\/\?]*)";

      // The rest of the path for a standard URL.
      $end = $directories . '?' . $query . '?' . $anchor . '?$/i';

      if (!preg_match($internal_pattern . $end, $uri)) {
        $link_domains = '[a-z][a-z0-9-]{1,62}';

        $authentication = "(?:(?:(?:[\w\.\-\+!$&'\(\)*\+,;=" . $link_ichars . "]|%[0-9a-f]{2})+(?::(?:[\w" . $link_ichars . "\.\-\+%!$&'\(\)*\+,;=]|%[0-9a-f]{2})*)?)?@)";
        $domain = '(?:(?:[a-z0-9' . $link_ichars . ']([a-z0-9' . $link_ichars . '\-_\[\]])*)(\.(([a-z0-9' . $link_ichars . '\-_\[\]])+\.)*(' . $link_domains . '|[a-z]{2}))?)';
        $ipv4 = '(?:[0-9]{1,3}(\.[0-9]{1,3}){3})';
        $ipv6 = '(?:[0-9a-fA-F]{1,4}(\:[0-9a-fA-F]{1,4}){7})';
        $port = '(?::([0-9]{1,5}))';

        // Pattern specific to external links.
        $external_pattern = '/^' . $authentication . '?(' . $domain . '|' . $ipv4 . '|' . $ipv6 . ' |localhost)' . $port . '?';
        if (preg_match($external_pattern . $end, $uri)) {
          return 'http://' . $uri;
        }
      }
    }

    // Add the internal: scheme and ensure a leading slash.
    return 'internal:/' . ltrim($uri, '/');
  }

}
