<?php

namespace Drupal\pt_migrate\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class DependencyEventSubscriber.
 *
 * Event subscribers for dependency imports.
 *
 * @package Drupal\pt_migrate
 */
class DependencyEventSubscriber implements EventSubscriberInterface {

  const MIGRATION_ID = 'pt_d7_node_dependency';

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity query.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $entityQuery;

  /**
   * DependencyEventSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   EntityType Manager.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   Entity Query service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, QueryFactory $entity_query) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityQuery = $entity_query;
  }

  /**
   * Get subscribed events.
   *
   * @inheritdoc
   */
  public static function getSubscribedEvents() {
    $events[MigrateEvents::POST_ROW_SAVE][] = ['onMigratePostRowSave'];
    return $events;
  }

  /**
   * Post import row actions, create and add paragraphs for dependency row.
   *
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   *   The import event object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onMigratePostRowSave(MigratePostRowSaveEvent $event) {
    if ($event->getMigration()->getBaseId() !== self::MIGRATION_ID) {
      return;
    }

    $row = $event->getRow();
    $source = $row->getSource();

    $provinceMapping = [
      '1' => 'Bocas del Toro',
      '2' => 'Coclé',
      '3' => 'Colón',
      '4' => 'Chiriquí',
      '5' => 'Darién',
      '6' => 'Herrera',
      '7' => 'Los Santos',
      '8' => 'Panamá',
      '9' => 'Veraguas',
      '10' => 'Comarca Ngobe Bugle',
      '11' => 'Comarca Guna Yala',
      '12' => 'Comarca Embera-Wounnan',
      'panama-oeste' => 'Panamá Oeste',
    ];

    $phoneParagraph = $this->entityTypeManager->getStorage('paragraph')
      ->create([
        'type' => 'phone_numbers',
        'field_phone_number_description' => '',
        'field_phone_number' => $source['field_phone'][0]['value'],
      ]);
    $phoneParagraph->save();

    $paragraph = $this->entityTypeManager->getStorage('paragraph')
      ->create([
        'type' => 'contact_information',
        'field_phone_numbers' => [
          'target_id' => $phoneParagraph->id(),
          'target_revision_id' => $phoneParagraph->getRevisionId(),
        ],
        'field_address' => [
          'value' => html_entity_decode(strip_tags($source['field_address'][0]['value'])),
        ],
        'field_province' => $provinceMapping[$source['field_province'][0]['tid']],
      ]);
    $paragraph->save();

    $node = $this->entityTypeManager
      ->getStorage('node')
      ->load((int) $row->getDestination()['nid']);
    $node->set('field_contact_information', [
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ]);
    $node->save();
  }

}
