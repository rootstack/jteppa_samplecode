<?php

namespace Drupal\pt_migrate\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\migrate\Event\MigrateRollbackEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Entity\Query\QueryFactory;

/**
 * Class FileEventSubscriber.
 *
 * Event subscribers for file imports.
 *
 * @package Drupal\pt_migrate
 */
class FileEventSubscriber implements EventSubscriberInterface {

  const MIGRATION_ID = 'pt_d7_files';

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity query.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $entityQuery;

  /**
   * FileEventSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   EntityType Manager.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   Entity Query service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, QueryFactory $entity_query) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityQuery = $entity_query;
  }

  /**
   * Get subscribed events.
   *
   * @inheritdoc
   */
  public static function getSubscribedEvents() {
    $events[MigrateEvents::POST_ROW_SAVE][] = ['onMigratePostRowSave'];
    $events[MigrateEvents::POST_ROLLBACK][] = ['onMigrateRollback'];
    return $events;
  }

  /**
   * Post import row actions, create an image media type for the images.
   *
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   *   The import event object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onMigratePostRowSave(MigratePostRowSaveEvent $event) {
    if ($event->getMigration()->getBaseId() !== self::MIGRATION_ID) {
      return;
    }

    $image_extensions = ['jpg', 'jpeg', 'png', 'gif'];

    $row = $event->getRow();
    $file_name = $row->getSourceProperty('filename');
    $extension = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));

    if (!in_array($extension, $image_extensions)) {
      return;
    }

    $media_storage = $this->entityTypeManager->getStorage('media');
    $media = $media_storage->create([
      'bundle' => 'image',
      'uid' => 1,
      'mid' => $row->getSourceProperty('fid'),
      'field_media_image' => ['target_id' => $row->getSourceProperty('fid')],
      'field_migrated' => TRUE,
    ]);
    $media->save();
  }

  /**
   * Removes all persons created by the migration.
   *
   * @param \Drupal\migrate\Event\MigrateRollbackEvent $event
   *   The rollback event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onMigrateRollback(MigrateRollbackEvent $event) {

    if ($event->getMigration()->getBaseId() !== self::MIGRATION_ID) {
      return;
    }

    // Delete migrated media entities.
    $entities = $this->entityQuery->get('media')
      ->condition('bundle', 'image')
      ->condition('field_migrated', TRUE)
      ->execute();
    if (!empty($entities)) {
      $node_storage = $this->entityTypeManager->getStorage('media');
      $entities = $node_storage->loadMultiple($entities);
      $node_storage->delete($entities);
    }
  }

}
