<?php

namespace Drupal\pt_online_services\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormBuilder;
use Drupal\facets\Utility\FacetsUrlGenerator;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $queryFactory;

  /**
   * Drupal\facets\Utility\FacetsUrlGenerator definition.
   *
   * @var \Drupal\facets\Utility\FacetsUrlGenerator
   */
  protected $facetsUrlGenerator;

  /**
   * Drupal\Core\Form\FormBuilder definition.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * Constructs a new DefaultController object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    QueryFactory $query_factory,
    FacetsUrlGenerator $facets_url_generator,
    FormBuilder $form_builder
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->queryFactory = $query_factory;
    $this->facetsUrlGenerator = $facets_url_generator;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity.query'),
      $container->get('facets.utility.url_generator'),
      $container->get('form_builder')
    );
  }

  /**
   * Build institution list.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return array
   *   Return render array.
   */
  public function build(Request $request) {
    $showCategory = NULL !== $request->query->get('category');

    if ($showCategory) {
      $content['categories']['view'] = $this->listCategories($request);
    }
    else {
      $content['online_services']['view'] = [
        '#type' => 'view',
        '#name' => 'online_services',
        '#display_id' => 'list',
      ];
    }

    return $content;
  }

  /**
   * List categories.
   *
   * @return array
   *   Return render array.
   */
  private function listCategories(Request $request) {
    $taxonomyStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $vid = 'sector';
    $filters = $this->getFilters($request->query);

    $sectors = $taxonomyStorage->loadTree($vid);
    $filteredSectors = $this->filterSectors($sectors, $filters);
    $builtSectors = $this->buildSectors($filteredSectors, $filters);
    $finalSectors = $this->removeEmptySectors($builtSectors);

    return [
      '#theme' => 'pt_online_services',
      '#sectors' => $finalSectors,
      '#attached' => [
        'library' => [
          'pt_online_services/pt-online-services-block',
        ],
      ],
    ];
  }

  /**
   * Get Filters from Query.
   *
   * @param Symfony\Component\HttpFoundation\ParameterBag $query
   *   Query.
   *
   * @return array
   *   Return filters in query.
   */
  private function getFilters(ParameterBag $query) {
    $f = $query->get('f') ?? [];
    $filters = [];
    foreach ($f as $parameter) {
      $data = explode(':', $parameter);
      $filters[$data[0]] = $data[1];
    }
    return $filters;
  }

  /**
   * Filter Sectors.
   *
   * @param array $sectors
   *   Sectors.
   * @param array $filters
   *   Filters.
   *
   * @return array
   *   Return filtered sectors.
   */
  private function filterSectors(array $sectors, array $filters) {
    return array_filter($sectors, function ($sector) use ($filters) {
      if (!isset($filters['sector'])) {
        return TRUE;
      }
      return $sector->tid === $filters['sector'];
    });
  }

  /**
   * Build Sectors as array.
   *
   * @param array $sectors
   *   Sectors.
   * @param array $filters
   *   Filters.
   *
   * @return array
   *   Return built sectors.
   */
  private function buildSectors(array $sectors, array $filters) {
    return array_map(function ($sector) use ($filters) {
      $query = $this->queryFactory->get('node')
        ->condition('type', 'online_service')
        ->condition('status', Node::PUBLISHED)
        ->condition('field_sector', $sector->tid);
      if (isset($filters['institucion'])) {
        $query->condition('field_institution', $filters['institucion']);
      }
      $onlineServiceAmount = $query->count()->execute();

      $url = $this->facetsUrlGenerator->getUrl(['sector' => [$sector->tid]], FALSE);
      $options = $url->getOptions();
      unset($options['query']['category']);
      $url = $url->setOptions($options);

      return [
        'onlineServiceAmount' => $onlineServiceAmount,
        'title' => $sector->name,
        'description' => $sector->description__value,
        'url' => $url->toString(),
      ];
    }, $sectors);
  }

  /**
   * Remove sector with zero institutions.
   *
   * @param array $sectors
   *   Built Sectors.
   *
   * @return array
   *   Return sectors that have at least one institution.
   */
  private function removeEmptySectors(array $sectors) {
    return array_filter($sectors, function ($sector) {
      return $sector['onlineServiceAmount'] > 0;
    });
  }

}
