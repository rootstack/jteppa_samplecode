<?php

namespace Drupal\pt_online_services\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CategoryForm.
 */
class CategoryForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'category_online_service_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['category'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Category'),
      '#attributes' => ['onchange' => 'this.form.submit();'],
    ];

    $form_state->setMethod('get');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // ..
  }

}
