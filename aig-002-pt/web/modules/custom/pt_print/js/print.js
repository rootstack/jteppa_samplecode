/* eslint func-names: ["error", "never"] */

(($, { behaviors }) => {
  behaviors.print = {
    attach(context) {
      $('.print_link', context).click(() => {
        window.print();
      });
    },
  };
})(jQuery, Drupal, drupalSettings);
