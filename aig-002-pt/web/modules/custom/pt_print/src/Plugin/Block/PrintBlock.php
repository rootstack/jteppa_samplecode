<?php

namespace Drupal\pt_print\Plugin\Block;

use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'Print' Block.
 *
 * @Block(
 *   id = "print_block",
 *   admin_label = @Translation("Print/Send by Email block"),
 *   category = @Translation("Print"),
 * )
 */
class PrintBlock extends BlockBase implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;
  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;
  /**
   * Drupal\Core\Block\BlockManager definition.
   *
   * @var \Drupal\Core\Block\BlockManager
   */
  protected $blockManager;
  /**
   * Drupal\Core\StringTranslation\TranslationInterface definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;
  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Constructs a new RelatedContentBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   AccountProxy Service.
   * @param \Drupal\Core\Block\BlockManager $block_manager
   *   The block manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   Translation Service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountProxy $current_user,
    BlockManager $block_manager,
    TranslationInterface $string_translation,
    CurrentRouteMatch $current_route_match
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->blockManager = $block_manager;
    $this->stringTranslation = $string_translation;
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('plugin.manager.block'),
      $container->get('string_translation'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $forwardBlock = $this->blockManager->createInstance('forward_link_block');
    $print = [
      '#markup' => '<a href="#" class="print_link">' . $this->t('Print') . '</a>' ,
      '#attached' => [
        'library' => [
          'pt_print/print',
        ],
      ],
    ];

    if (!$forwardBlock->isAllowed()) {
      return $print;
    }

    return [
      'print' => [
        '' => $print,
        '#prefix' => '<div class="print">',
        '#suffix' => '</div>',
      ],
      'forward' => [
        '' => $forwardBlock->build(),
        '#prefix' => '<div class="forward">',
        '#suffix' => '</div>',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    if ($node = $this->currentRouteMatch->getParameter('node')) {
      return Cache::mergeTags(parent::getCacheTags(), ['node:' . $node->id()]);
    }
    else {
      return parent::getCacheTags();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

}
