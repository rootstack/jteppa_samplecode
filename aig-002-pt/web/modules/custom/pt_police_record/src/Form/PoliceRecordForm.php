<?php

namespace Drupal\pt_police_record\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PoliceRecordForm.
 */
class PoliceRecordForm extends FormBase {

  /**
   * Record policivo URL.
   */
  const PR_URL = 'https://panamaenlinea.gob.pa/PELBack/rest/pel/p013_03/checkRecordValido';

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new PoliceRecordForm object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger('pt_police_record');
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
  }

  /**
   * Create DI Method.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Class container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'police_record_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['police_record_logo'] = [
      '#prefix' => '<div class="police-record-logo-container">',
      '#suffix' => '</div>',
      '#theme' => 'image',
      '#uri' => drupal_get_path('module', 'pt_police_record').'/police-record-logo.jpg',
      '#weight' => 0,
    ];
    
    $form['request_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Request Number'),
      '#required' => TRUE,
      '#maxlength' => 255,
      '#size' => 64,
      '#weight' => 1,
    ];

    $form['unique_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Unique Code'),
      '#required' => TRUE,
      '#maxlength' => 255,
      '#size' => 64,
      '#weight' => 2,
    ];

    $form['actions'] = array(
      '#type' => 'actions',
      '#weight' => 3,
    );

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Verify'),
    ];

    $form['actions']['reset'] = [
      '#type' => 'html_tag',
      '#tag' => 'input',
      '#attributes' => [
        'class' => 'button',
        'type' => 'reset',
        'value' => 'Clear',
      ],
    ];

    if ($form_state->getValue('search_found') === TRUE) {
      $form['result'] = [
        '#theme' => 'pt_record_result',
        '#name' => $form_state->getValue('name'),
        '#request_number' => $form_state->getValue('request_number'),
        '#unique_code' => $form_state->getValue('unique_code'),
        '#expedition_date' => $form_state->getValue('expedition_date'),
        '#province' => $form_state->getValue('city'),
        '#expired' => $form_state->getValue('expired'),
        '#weight' => 4,
      ];

      $form_state->setUserInput([]);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $status = NULL;
    $result = NULL;
    $request_number = $values['request_number'];
    $unique_code = $values['unique_code'];

    try {
      // Guzzle does not play nice with this endpoint, had to fallback to curl.
      $ch = curl_init();
      $payload = json_encode([
        'codigoUnico' => $unique_code,
        'numeroSolicitud' => $request_number,
      ]);
      curl_setopt($ch, CURLOPT_URL, self::PR_URL);
      curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-type: application/json']);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      $result = curl_exec($ch);
      $status = curl_getinfo($ch);
      curl_close($ch);

      $result = json_decode($result);

    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      $this->messenger->addError($this->t('An error has occurred, try again later.'));
      $form_state->setRebuild();
    }

    if (empty($result)) {
      return;
    }

    if (!empty($result->status) && $result->status === 'BAD_REQUEST') {
      foreach ($result->errors as $error) {
        $this->messenger->addError($error);
      }
    }
    elseif ($status['http_code'] === 200 && empty($result->nombre)) {
      $this->messenger->addError($this->t('Request not found, please check your data and try again.'));
    }
    elseif ($status['http_code'] === 200 && !empty($result->nombre)) {
      $form_state->setValue('search_found', TRUE);
      $form_state->setValue('name', $result->nombre);
      $form_state->setValue('request_number', $result->numeroSolicitud);
      $form_state->setValue('unique_code', $result->codigoUnico);
      $form_state->setValue('expedition_date', $result->fecha_expedicion);
      $form_state->setValue('city', $result->ciudad);
      $form_state->setValue('expired', $result->expirado);
    }

    $form_state->setRebuild();

  }

}
