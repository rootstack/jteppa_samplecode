<?php

namespace Drupal\pt_faq\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormState;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FaqController.
 *
 * @package Drupal\pt_faq\Controller
 */
class FaqController extends ControllerBase {

  /**
   * Constructor.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Shows FAQ main page.
   *
   * @return array
   *   Return a Render Array.
   */
  public function index() {
    $response = [];

    // Search bar for FAQ results.
    $form = [];
    $view_id = 'faq_list';
    $display_id = 'faq_results_page';
    $view = Views::getView($view_id);
    if ($view) {
      $view->setDisplay($display_id);
      $view->initHandlers();
      $form_state = (new FormState())
        ->setStorage([
          'view' => $view,
          'display' => &$view->display_handler->display,
          'rerender' => TRUE,
        ])
        ->setMethod('get')
        ->setAlwaysProcess()
        ->disableRedirect();
      $form_state->set('rerender', NULL);
      $form = \Drupal::formBuilder()->buildForm('\Drupal\views\Form\ViewsExposedForm', $form_state);
      if (!empty($form['field_categories'])) {
        unset($form["field_categories"]);
      }
      if (!empty($form["#info"]["filter-field_categories"])) {
        unset($form["#info"]["filter-field_categories"]);
      }
      if (isset($form["#info"]["filter-search_api_fulltext"]["label"])) {
        $form["#info"]["filter-search_api_fulltext"]["label"] = '';
      }
    }
    $response['search_form'] = $form;

    // FAQ Categories section.
    $response['faq_categories'] = [
      '#type' => 'view',
      '#name' => 'faq_categories',
      '#display_id' => 'faq_categories_block',
      '#arguments' => [],
    ];

    // Highlighted questions section.
    $response['highlighted_questions'] = [
      '#type' => 'view',
      '#name' => 'highlighted_questions',
      '#display_id' => 'hl_questions_blk',
      '#arguments' => [],
    ];

    return $response;
  }

}
