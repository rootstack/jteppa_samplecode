<?php

namespace Drupal\pt_statistics\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node\Entity\Node;
use Drupal\pt_base\Service\NodeSummaryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'StatisticsBlock' block.
 *
 * @Block(
 *  id = "statistics_block",
 *  admin_label = @Translation("Statistics block"),
 * )
 */
class StatisticsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\pt_base\Service\NodeSummaryInterface definition.
   *
   * @var \Drupal\pt_base\Service\NodeSummaryInterface
   */
  protected $nodeSummary;

  /**
   * Constructs a new StatisticsBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\pt_base\Service\NodeSummaryInterface $node_summary
   *   Node summary definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    NodeSummaryInterface $node_summary
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->nodeSummary = $node_summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('node_summary')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $nodesData = [
      NodeSummaryInterface::INSTITUTION => ['name' => $this->t('Institutions')],
      NodeSummaryInterface::DEPENDENCY => ['name' => $this->t('Dependencies')],
      NodeSummaryInterface::PROCEDURE => ['name' => $this->t('Procedures')],
      NodeSummaryInterface::ARTICLE => ['name' => $this->t('Articles')],
      NodeSummaryInterface::ONLINE_SERVICE => ['name' => $this->t('Online Services')],
    ];

    foreach ($nodesData as $contentType => $nodeData) {
      $nodesData[$contentType]['published'] = $this->nodeSummary->getNodeAmount($contentType, Node::PUBLISHED);
      $nodesData[$contentType]['not_published'] = $this->nodeSummary->getNodeAmount($contentType, Node::NOT_PUBLISHED);
    }

    $nodesData[NodeSummaryInterface::ONLINE_PROCEDURE] = [
      'name' => $this->t('Online Procedures'),
      'published' => $this->nodeSummary->getNodeAmount(NodeSummaryInterface::ONLINE_PROCEDURE, Node::PUBLISHED),
    ];

    $build = [
      '#theme' => 'pt_statistics',
      '#nodesData' => $nodesData,
    ];

    return $build;
  }

}
