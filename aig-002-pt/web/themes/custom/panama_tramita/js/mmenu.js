/* eslint-disable */
(function ($, Drupal) {
  Drupal.behaviors.mobileMenuBehavior = {
    attach: function (context, settings) {
      var mMenuApi = $("#off-canvas", context).once('mmenuDataBehavior').data("mmenu");
      $(window, context).once('windowResizeBehavior').on('resize', function() {
        if ($(this).width() >= 809) {
          mMenuApi.close();
        }
      });
    }
  };
})(jQuery, Drupal);