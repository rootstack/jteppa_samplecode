/* eslint-disable */
(function ($, Drupal) {
  Drupal.behaviors.institutionProfileBehavior = {
    attach: function (context, settings) {

      $(".node--view-mode-full .body-container .view-more-link", context).once('expandDescriptionBehavior').on("click", function () {
        $(".body-container", context).addClass("full");
        $(".short-body", context).stop(true, true).animate({'maxHeight': $(".short-body .field--name-body").height()}, 1000);
        return false;
      });
    
      $(".node--view-mode-full .body-container .view-less-link", context).once('collapseDescriptionBehavior').on("click", function () {
        $(".body-container", context).removeClass("full");
        $(".short-body", context).stop(true, true).animate({'maxHeight': 370}, 1000);
        return false;
      });
    
      $(".paragraph--view-mode--institution-telephones .field__label", context).once('slideTogglePhonesBehavior').on("click", function () {
        $(this).toggleClass("open");
        $(".paragraph--view-mode--institution-telephones .field__items", context).stop(true, true).slideToggle();
      });
    }
  };
})(jQuery, Drupal);