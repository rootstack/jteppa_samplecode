/* eslint-disable */
(function ($, Drupal) {
  Drupal.behaviors.menuSearchBehavior = {
    attach: function (context, settings) {
      $(".l-navigation .search-icon", context).once('slideToggleSearchIconBehavior').on("click", function () {
        $(this, context).next().stop(true, true).slideToggle();
      });
    }
  };
})(jQuery, Drupal);