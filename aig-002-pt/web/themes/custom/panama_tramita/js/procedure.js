/* eslint-disable */
(function ($, Drupal) {
  Drupal.behaviors.procedureProfileBehavior = {
    attach: function (context, settings) {
      $(".collapsable > .field__label, .collapsable > .field > .field__label", context).once('slideToggleFieldBehavior').on("click", function () {
        $(this).toggleClass("open");
        $(this, context).next().stop(true, true).slideToggle();
      });

      var slideElements = ".field--name-field-requirement-type,"+
      ".field--name-field-procedure-applies > .field__label,"+
      ".field--name-field-procedure-costs > .field__label,"+
      ".field--name-field-generated-document > .field__label,"+
      ".field--name-field-duration > .field__label";

      $(slideElements, context).once('slideToggleFieldBehavior').on("click", function () {
        $(this).toggleClass("open");
        $(this, context).next().stop(true, true).slideToggle();
      });
      
    }
  };
})(jQuery, Drupal);