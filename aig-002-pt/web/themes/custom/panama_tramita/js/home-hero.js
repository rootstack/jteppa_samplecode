/* eslint-disable */
(function ($, Drupal) {
  Drupal.behaviors.homeHeroBehavior = {
    attach: function (context, settings) {
      $('body', context).once('once-name').each(function () {
        var n = $(".view-home-hero .views-row", context).length;
        if (n > 1) {
          var i = 0;
          var homeHeroInterval = setInterval(function() {
            $(".view-home-hero .views-row", context).eq(i).fadeOut(400, function() {
              if (i == (n-1)) {
                i = 0;
              }
              else {
                i++;
              }
              $(".view-home-hero .views-row", context).eq(i).fadeIn(400);
            });
          }, 30000);
        }
      });
    }
  };
})(jQuery, Drupal);